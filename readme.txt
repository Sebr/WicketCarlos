Boletines

Integrantes: Cristina, Luz, Natalia, Sebastián

El objetivo es mostrar el boletín de un alumno de secundaria.
Cada alumno pertenece a un curso, en cada curso se dicta una cantidad de materias.
Los cursos son anuales (por ejemplo: 2º año, división C, turno mañana).
Para cada materia se registran 3 notas, una para cada trimestre.
Cada materia tiene una condición de aprobación, que es la misma para todos los cursos.


Proyecto generado con Apache Wicket Quickstart
https://wicket.apache.org/start/quickstart.html


Online en Heroku:
https://calm-crag-44048.herokuapp.com/

