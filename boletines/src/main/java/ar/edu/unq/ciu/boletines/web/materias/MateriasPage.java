package ar.edu.unq.ciu.boletines.web.materias;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.ciu.boletines.dominio.Materia;

//página para ver la lista de materias, y realizar altas
public class MateriasPage extends WebPage {

	private static final long serialVersionUID = -496368141402136321L;
	
	private MateriasPageControlador ctrl;
	
	// constructores
	public MateriasPage() {
		super();
		
		this.ctrl = new MateriasPageControlador();
		
		this.configurarPagina();
	}

	public MateriasPage(MateriasPageControlador ctrl) {
		super();
		
		this.ctrl = ctrl;
		
		this.configurarPagina();
	}

	// carga de widgets
	private void configurarPagina() {
		this.add(new ListView<Materia>("materias", new PropertyModel<>(this.ctrl, "materias")) {
			private static final long serialVersionUID = 8019544445797746427L;

			@Override
			protected void populateItem(ListItem<Materia> item) {
				Materia materia = item.getModelObject();
				
				item.add(new Label("nombre", new PropertyModel<>(materia, "nombre")));
				item.add(new Label("condAprobacion", new PropertyModel<>(materia, "condicionStrategy.nombreCondAprobacion")));
			}
		});
		
		this.add(new Link<String>("agregarMateria") {
			private static final long serialVersionUID = -5996731022366911614L;

			@Override
			public void onClick() {
				MateriasPage.this.ctrl.resetMateria();
				this.setResponsePage(new AgregarMateriaForm(MateriasPage.this.ctrl));
			}
		});
	}
	
}
