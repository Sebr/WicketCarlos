package ar.edu.unq.ciu.boletines.logIn;

import java.io.Serializable;

import ar.edu.unq.ciu.boletines.ProfesorStore.StoreProf;

public class LoginController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String userName;
	private String password;
	private StoreProf store = StoreProf.getInstancia();
	private String status = "";

	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public StoreProf getStore() {
		return store;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public LoginController() {
		super();
	}
	
	
}
