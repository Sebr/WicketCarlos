package ar.edu.unq.ciu.boletines.dominio;

import java.io.Serializable;
import java.util.List;

public class Materia implements Serializable {

	private static final long serialVersionUID = 5604545035178943511L;
	
	// atributos
	private String nombre;
	private StrategyConditionResolution condicionStrategy;
	
	// constructores
	public Materia() {
		super();
	}
	
	public Materia(String nombre, StrategyConditionResolution condicionStrategy) {
		super();
		this.nombre = nombre;
		this.condicionStrategy = condicionStrategy;
	}

	// getters y setters	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public StrategyConditionResolution getCondicionStrategy() {
		return this.condicionStrategy;
	}
	
	public void setCondicionStrategy(StrategyConditionResolution strategy) {
		this.condicionStrategy = strategy;
	}

	// queries
	
	public boolean estaAprobada(List<Integer> notas) {
		return this.condicionStrategy.estaAprobada(notas);
	}
	
	public boolean estaDesaprobada(List<Integer> notas) {
		return this.condicionStrategy.estaDesaprobada(notas);
	}

	public boolean leFaltaUnaNota(List<Integer> notas) {
		return this.condicionStrategy.leFaltaUnaNota(notas);
	}
	
	public int queNotaNecesitaParaAprobar(List<Integer> notas) {
		return this.condicionStrategy.queNotaNecesitaParaAprobar(notas);
	}

}
