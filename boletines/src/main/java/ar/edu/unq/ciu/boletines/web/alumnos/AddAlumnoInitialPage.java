package ar.edu.unq.ciu.boletines.web.alumnos;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.validation.IFormValidator;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.ciu.boletines.dominio.Alumno;
import ar.edu.unq.ciu.boletines.dominio.Curso;

public class AddAlumnoInitialPage extends WebPage {

	
	
	private static final long serialVersionUID = -1484614197143221017L;

	
	
	AlumnoAddController controller;

	public  AddAlumnoInitialPage(AlumnoAddController _controller) { 
		this.controller = _controller;
		this.add(this.buildForm());
	}

	public Form<AlumnoAddController> buildForm() {
		Form<AlumnoAddController> studentMainDataForm = new Form<AlumnoAddController>("addAlumno") {

			private static final long serialVersionUID = 2995680129929377838L;

			protected void onSubmit() {
				AddAlumnoInitialPage.this.controller.doAddAlumno();
				this.setResponsePage(AlumnoPage.class);
			}
		};

		PropertyModel<Curso> pmalumno = new PropertyModel<>(this.controller, "cursoSeleccionado");

		studentMainDataForm.add(new TextField<String>("lastName", new PropertyModel<>(this.controller, "apellido")));
		studentMainDataForm.add(new TextField<String>("name", new PropertyModel<>(this.controller, "nombre")));
		studentMainDataForm.add(new NumberTextField<Integer>("dni", new PropertyModel<>(this.controller, "dni")));
		studentMainDataForm.add(new TextField<String>("mail", new PropertyModel<>(this.controller, "mail")));
		studentMainDataForm.add(new NumberTextField<Integer>("telefono", new PropertyModel<>(this.controller, "telefono")));
		studentMainDataForm.add(new DropDownChoice<>("cursoSeleccionado", pmalumno, new PropertyModel<>(this.controller, "cursos"), new ChoiceRenderer<>("anioDivisionYTurno")));
		;
		return studentMainDataForm;
		
	}
}

