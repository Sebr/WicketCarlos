package ar.edu.unq.ciu.boletines.calificaciones;

import java.util.List;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.ciu.boletines.dominio.Alumno;
import ar.edu.unq.ciu.boletines.dominio.Materia;
import ar.edu.unq.ciu.boletines.dominio.Profesor;

public class StudentsQualifyPage extends WebPage{

	private static final long serialVersionUID = 1L;	
    
    private StudentsQulifyPageController controller = new StudentsQulifyPageController();
    
	public StudentsQualifyPage(List<Alumno> alumnosQueHayqueCalificar, Materia materia, String nombreTrimestre, Profesor logueado) {
		super();
		this.add(buildQualifyButtonForm());
		this.controller.setSubjectToShow(materia);
		this.controller.setTrimester(nombreTrimestre);
		this.controller.setStudentsToQualify(alumnosQueHayqueCalificar);
		this.controller.setLoggedProfessor(logueado);
		this.controller.fillListForms( materia, nombreTrimestre);
		this.add(new Label("subjectToShow", new PropertyModel<String>(this.controller,"subjectToShow.nombre")));
		this.add(new Label("trimesterToShow", new PropertyModel<String>(this.controller,"trimester")));
			
	}
	
	
	  Button button1 = new Button("button1") {

		private static final long serialVersionUID = 1L;

			public void onSubmit() {
	        	this.setResponsePage(new FormSelectCoursePage(StudentsQualifyPage.this.controller.getLoggedProfessor()));
	        	
	        }
	    };
	    
	    Button button2 = new Button("button2") {

			private static final long serialVersionUID = 1L;

				public void onSubmit() {
		        	this.setEnabled(false);
		        	
		        }
		    };
 
   public Form<StudentQualifyFormController> buildQualifyButtonForm(){
    	
    	Form<StudentQualifyFormController> newForm = new Form<StudentQualifyFormController>("mainForm") {

	
			private static final long serialVersionUID = 1L;

			protected void onSubmit() {
				 StudentsQualifyPage.this.controller.qualifyAll();
   		  }	 
			
    	};
    
    	ListView<Form<StudentQualifyFormController>> lv =  new ListView<Form<StudentQualifyFormController>>("students", this.controller.getFormList()){
      		
  			private static final long serialVersionUID = 1L;

  			@Override
  			protected void populateItem(ListItem<Form<StudentQualifyFormController>> item) {
  				
  				  item.add(item.getModelObject());		 
  			}
      		
      		
      	};
      	
      	newForm.add(button1);
      	newForm.add(button2);
      	newForm.add(lv);
    	return newForm;
    }
    
}
