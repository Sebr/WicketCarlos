package ar.edu.unq.ciu.boletines.calificaciones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unq.ciu.boletines.dominio.Curso;
import ar.edu.unq.ciu.boletines.dominio.Profesor;

public class FormSelectCoursePageController  implements Serializable{

	private static final long serialVersionUID = 1L;
	

     private Curso chosenCourse;
     private List<Curso> coursesToChose = new ArrayList<>();
 	 private Profesor loggedProfessor;

	public Profesor getLoggedProfessor() {
		return loggedProfessor;
	}

	public void setLoggedProfessor(Profesor lp) {
		this.loggedProfessor = lp;
	}

	public List<Curso> getCoursesToChose() {
		return coursesToChose;
	}

	public void setCoursesToChose(List<Curso> ctc) {
		this.coursesToChose = ctc;
	}

	public Curso getChosenCourse() {
		return chosenCourse;
	}

	public void setChosenCourse(Curso ctc) {
		this.chosenCourse = ctc;
	}

	public void AddCourse(Curso course) {
		this.getCoursesToChose().add(course);
	}
     

// constructor
    public FormSelectCoursePageController(){
		super();
	}


}
