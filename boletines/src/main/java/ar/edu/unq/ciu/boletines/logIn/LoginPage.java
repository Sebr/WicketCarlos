package ar.edu.unq.ciu.boletines.logIn;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.ciu.boletines.altasProfesores.AddProfessorPage;
import ar.edu.unq.ciu.boletines.calificaciones.FormSelectCoursePage;

public class LoginPage extends WebPage {

	private static final long serialVersionUID = 1L;
	
	private LoginController controller = new LoginController();
	
	
	public LoginPage() {
		super();
		this.add(this.buildForm());	
	}
	
	public Form<LoginController> buildForm(){
		
		Form<LoginController> newForm = new Form<LoginController>("loginForm") {
					
			private static final long serialVersionUID = 1L;
			
			 protected void onSubmit() {
				 
				 if(LoginPage.this.controller.getStore().userAlreadyExists(LoginPage.this.controller.getUserName(),LoginPage.this.controller.getPassword())) {
					 
				   this.setResponsePage(new FormSelectCoursePage(
						   LoginPage.this.controller.getStore().getProfessor(LoginPage.this.controller.getUserName())));
				   
				 }else {
					 LoginPage.this.controller.setStatus("Usuario o contraseña incorrecta"); 
					 
				 }
			  }		
			
		};
		
		
		Link<String> ls =  new Link<String>("createAccount") {
			private static final long serialVersionUID = -7850955536756349914L;

			@Override
			public void onClick() {
				this.setResponsePage(new AddProfessorPage());
				
			}
		};
		
		
		PasswordTextField newPassTextField = new PasswordTextField("pass", new PropertyModel<> (this.controller,"password"));
		TextField<String> newTextField = new TextField<String>("user", new PropertyModel<>(this.controller, "userName"));
		
		newPassTextField.setRequired(true);
		newTextField.setRequired(true);
		newForm.add(newTextField);
		newForm.add(newPassTextField);
		newForm.add(new Label("loginStatus", new PropertyModel<>(this.controller,"status")));
		newForm.add(ls);
		return newForm;
	}
	
	
	
}
