package ar.edu.unq.ciu.boletines.web.alumnos;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.ciu.boletines.dominio.Alumno;
import ar.edu.unq.ciu.boletines.dominio.store.BoletinesStore;

public class AlumnoPage extends WebPage  {
	
	
	private static final long serialVersionUID = -8075124524413427802L;
	AlumnoAddController controller;

	public AlumnoPage() {
		super();
		this.controller = new AlumnoAddController();
		this.fillStudentTable();
		this.fillAddStudentButton();
	}

	private void fillAddStudentButton() {
		Link<String> addStudentAction = new Link<String>("addStudent") {
			

			private static final long serialVersionUID = -5136322466582064489L;
		
			

			@Override
			public void onClick() {
				this.setResponsePage(new AddAlumnoInitialPage(AlumnoPage.this.controller));
				
			}
		}; 
		this.add(addStudentAction);
	}

	private void fillStudentTable() {
		ListView<Alumno> listTable = new ListView<Alumno>("alumnos", new PropertyModel<>(this, "alumnos")) {
			
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Alumno> panel) {
				CompoundPropertyModel<Alumno> alumnoModel = new CompoundPropertyModel<>(panel.getModelObject());
				panel.add(new Label("lastName", alumnoModel.bind("apellido")));
				panel.add(new Label("name", alumnoModel.bind("nombre")));
				panel.add(new Label("dni", alumnoModel.bind("dni")));
				panel.add(new Label("mail", alumnoModel.bind("mail")));
				panel.add(new Label("telefono", alumnoModel.bind("telefono")));
				panel.add(new Label("curso", alumnoModel.bind("curso.nombre")));
			
				
			}

			
		}; 
		
        this.add(listTable);
}
		
	public List<Alumno> getAlumnos() {
		return BoletinesStore.getInstancia().getAlumnos().stream()
				.collect(Collectors.toList());
	}
}
		

	




