package ar.edu.unq.ciu.boletines.web.api;

import ar.edu.unq.ciu.boletines.dominio.Curso;

public class CursoDataObject {

	private String anio;
	private String division;
	private String turno;
	private int id;

	
	public CursoDataObject(Curso curso ) {
		super();
		this.anio =  Integer.toString(curso.getAnio());                
		this.division = curso.getDivision();
		this.turno = curso.getTurno();
		this.id = curso.getId();
		
	}
	
	
	
}
