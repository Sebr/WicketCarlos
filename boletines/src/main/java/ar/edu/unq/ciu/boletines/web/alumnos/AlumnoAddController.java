package ar.edu.unq.ciu.boletines.web.alumnos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unq.ciu.boletines.dominio.Alumno;
import ar.edu.unq.ciu.boletines.dominio.Curso;
import ar.edu.unq.ciu.boletines.dominio.store.BoletinesStore;

public class AlumnoAddController implements Serializable {
	
	private static final long serialVersionUID = 316673469073217127L;
	
    public String apellido;
    public String nombre;
    public String dni;
    public String mail;
    public String telefono;
    private List<Curso>cursos = new ArrayList<>();
    private Curso cursoSeleccionado;
    
	public AlumnoAddController(){
 		super();
  		this.cursos = BoletinesStore.getInstancia().getCursos();
  		
 	}

    public AlumnoAddController(String apellido, String nombre, String dni,String mail, String telefono) {
 		this.apellido = apellido;
		this.nombre= nombre;
  		this.dni = dni;
  		this.mail = mail;
  		this.telefono = telefono;
  	}
     
    public Curso getCursoSeleccionado() {
		return cursoSeleccionado;
	}
	public void setCursoSeleccionado(Curso curso) {
		this.cursoSeleccionado = curso;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
     
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getApellido() {
		return apellido;
	}

	public void setLastName(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	
	public void doAddAlumno() {
		Alumno newAlumno = new Alumno();
		newAlumno.setApellido(apellido);
		newAlumno.setNombre(nombre);
		newAlumno.setDni(dni);
		newAlumno.setMail(mail);
		newAlumno.setTelefono(telefono);
		newAlumno.setCurso(cursoSeleccionado);
		this.cursoSeleccionado.agregarAlumno(newAlumno);// Faltaba este enganche          
		BoletinesStore.getInstancia().addAlumno(newAlumno);
				
	}
	public List<Curso> getCursos() {
		return cursos;
	}
	
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}


	
}


	

