package ar.edu.unq.ciu.boletines.web.materias;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import ar.edu.unq.ciu.boletines.dominio.Materia;
import ar.edu.unq.ciu.boletines.dominio.StrategyConcreteCRA;
import ar.edu.unq.ciu.boletines.dominio.StrategyConcreteCRB;
import ar.edu.unq.ciu.boletines.dominio.StrategyConcreteCRC;
import ar.edu.unq.ciu.boletines.dominio.StrategyConditionResolution;
import ar.edu.unq.ciu.boletines.dominio.store.BoletinesStore;

//controller para MateriasPage y AgregarMateriaForm
public class MateriasPageControlador implements Serializable {

	private static final long serialVersionUID = -6050148889153525051L;
	
	private BoletinesStore bdas;
	private String nombreDeMateria;
	private StrategyConditionResolution condicionDeAprobacion;
	
	// constructor
	public MateriasPageControlador() {
		super();
		this.initStore();
	}

	// getters y setters
	public void resetMateria() {
		this.nombreDeMateria = null;
		this.condicionDeAprobacion = null;
	}

	public Collection<Materia> getMaterias() {
		return this.bdas.getMaterias();
	}
	
	public Collection<StrategyConditionResolution> getCondiciones() {
		return new ArrayList<>(
				Arrays.asList(StrategyConcreteCRA.getInstancia(), StrategyConcreteCRB.getInstancia(),
								StrategyConcreteCRC.getInstancia()));
	}
	
	public String getNombreDeMateria() {
		return nombreDeMateria;
	}

	public void setNombreDeMateria(String nombreMateria) {
		this.nombreDeMateria = nombreMateria;
	}

	public StrategyConditionResolution getCondicionDeAprobacion() {
		return condicionDeAprobacion;
	}

	public void setCondicionDeAprobacion(StrategyConditionResolution condicionDeAprobacion) {
		this.condicionDeAprobacion = condicionDeAprobacion;
	}

	// altas
	public void agregarMateria() {
		this.bdas.addMateria(new Materia(this.nombreDeMateria, this.condicionDeAprobacion));
	}
	
	// inicializar el store
	private void initStore() {
		this.bdas = BoletinesStore.getInstancia();
		this.bdas.cargarDatosPrueba();
	}

}
