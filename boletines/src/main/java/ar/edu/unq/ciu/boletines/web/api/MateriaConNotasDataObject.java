package ar.edu.unq.ciu.boletines.web.api;

import ar.edu.unq.ciu.boletines.web.boletines.MateriaConNotas;

public class MateriaConNotasDataObject {

	private String nombre;
	private Integer nota1;
	private Integer nota2;
	private Integer nota3;
	private String condicion;

	// constructor
	
	public MateriaConNotasDataObject(MateriaConNotas mcn) {
		super();
		this.nombre = mcn.getMateria().getNombre();
		this.nota1 = mcn.getNota1();
		this.nota2 = mcn.getNota2();
		this.nota3 = mcn.getNota3();
		this.condicion = mcn.getCondicion();
	}
	
	// getters

	public String getNombre() {
		return nombre;
	}

	public Integer getNota1() {
		return nota1;
	}

	public Integer getNota2() {
		return nota2;
	}

	public Integer getNota3() {
		return nota3;
	}

	public String getCondicion() {
		return condicion;
	}
	
}
