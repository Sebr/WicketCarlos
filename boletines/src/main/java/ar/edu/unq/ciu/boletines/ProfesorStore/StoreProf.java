package ar.edu.unq.ciu.boletines.ProfesorStore;

import java.util.ArrayList;
import java.util.List;

import ar.edu.unq.ciu.boletines.dominio.Profesor;

public class StoreProf {
	
	private static StoreProf instancia;
	private List<Profesor> profesores = new ArrayList<>();	
	
	
	public List<Profesor> getProfesores() {
		return profesores;
	}

	public void agregarProfesor(Profesor prof) {
		this.profesores.add(prof);
	}
	
	
	private StoreProf() {
		super();
	}	
	
	
	
	public boolean userAlreadyExists(String userName, String pass) {
		return this.getProfesores().stream().anyMatch(p -> p.getUserName().equals(userName) && p.getPassword().equals(pass));
	}
	
	public Profesor getProfessor(String userName) {
		return this.getProfesores().stream().
			filter( p -> p.getUserName().equals(userName)).findAny().get();                                    
	}
	
	
	public static StoreProf getInstancia() {
		if (instancia == null) {
			instancia = new StoreProf();
		}
		return instancia;
	}

}
