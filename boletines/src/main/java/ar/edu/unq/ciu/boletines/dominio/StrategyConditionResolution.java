package ar.edu.unq.ciu.boletines.dominio;

import java.util.List;

// Strategy para resolver las condiciones para aprobar una materia
public abstract class StrategyConditionResolution {
	
	public abstract boolean estaAprobada(List<Integer> notas);
	
	public abstract boolean estaDesaprobada(List<Integer> notas);
	
	public abstract int queNotaNecesitaParaAprobar(List<Integer> notas); 
	
	public abstract String getNombreCondAprobacion();
	
	public abstract String getDescripcionCondAprobacion();
	
	protected boolean tieneTodasLasNotas(List<Integer> notas) {
		return this.cuantasNotasTiene(notas) == 3;
	}
	
	public boolean leFaltaUnaNota(List<Integer> notas) {
		return this.cuantasNotasTiene(notas) == 2;
	}
	
	protected long cuantasNotasTiene(List<Integer> notas) {
		return notas.stream()
				.filter(nota -> nota != null)
				.count();
	}

	protected int sumaDeNotas(List<Integer> notas) {
		return notas.stream()
				.filter(nota -> nota != null)
				.mapToInt(nota -> nota)
				.sum();
	}
	
	protected boolean sonTodasLasNotasMayoresOIgualesQue(List<Integer> notas, int nota_min) {
		return notas.stream()
				.filter(nota -> nota != null)
				.allMatch(nota -> nota >= nota_min);
	}
	
}
