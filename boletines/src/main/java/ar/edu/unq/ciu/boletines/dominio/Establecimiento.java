package ar.edu.unq.ciu.boletines.dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Establecimiento implements Serializable {

	private static final long serialVersionUID = 2153575768378033100L;
	
	// atributos
	private List<Curso> cursos = new ArrayList<>();
	private List<Alumno> alumnos = new ArrayList<>();
	private List<Materia> materias = new ArrayList<>();

	// getters
	public List<Alumno> getAlumnos() {
		return alumnos;
	}

	public List<Curso> getCursos() {
		return this.cursos;
	}

	public List<Materia> getMaterias(){
		return this.materias;
	}
	
	// altas
	public void addAlumno(Alumno newAlumno) {
		this.alumnos.add(newAlumno);
	}

	public void addCurso(Curso curso){
		this.cursos.add(curso);
	}
	
	public void addMateria(Materia materia){
		this.materias.add(materia);
	}

}
