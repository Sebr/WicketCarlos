package ar.edu.unq.ciu.boletines.dominio;

import java.io.Serializable;
import java.util.List;

//Strategy concreto para resolver las condiciones para aprobar una materia
//caso 'B': hay que sumar 19 puntos entre las tres notas, todas tienen que ser 4 o más, no puede haber más de una que baje de 6
public class StrategyConcreteCRB extends StrategyConditionResolution implements Serializable {

	private static final long serialVersionUID = 400376665880079454L;

	private static StrategyConcreteCRB instancia;
	
	private StrategyConcreteCRB() {
		super();
	}
	
	public static StrategyConcreteCRB getInstancia() {
		if (StrategyConcreteCRB.instancia == null) {
			StrategyConcreteCRB.instancia = new StrategyConcreteCRB();
		}
		return StrategyConcreteCRB.instancia;
	}
	
	@Override
	public boolean estaAprobada(List<Integer> notas) {
		return this.tieneTodasLasNotas(notas)
				&& this.sumaDeNotas(notas) >= 19
				&& this.sonTodasLasNotasMayoresOIgualesQue(notas, 4)
				&& this.cantNotasMenoresQue(notas, 6) <= 1;
	}

	@Override
	public boolean estaDesaprobada(List<Integer> notas) {
		return (this.cuantasNotasTiene(notas) == 2
					&& this.queNotaNecesitaParaAprobar(notas) > 10)
				|| (this.cuantasNotasTiene(notas) > 0
					&& (!this.sonTodasLasNotasMayoresOIgualesQue(notas, 4)
						|| this.cantNotasMenoresQue(notas, 6) > 1))
				|| (this.tieneTodasLasNotas(notas)
					&& this.sumaDeNotas(notas) < 19);
	}

	@Override
	public int queNotaNecesitaParaAprobar(List<Integer> notas) {
		int nota = 19 - this.sumaDeNotas(notas);
		int notaMin = 4;
		if (this.cantNotasMenoresQue(notas, 6) > 0) {
			notaMin = 6;
		}
		return Math.max(nota, notaMin);
	}

	public long cantNotasMenoresQue(List<Integer> notas, int nota_max) {
		return notas.stream()
				.filter(nota -> ((nota != null) && (nota < nota_max)))
				.count();
	}

	@Override
	public String getNombreCondAprobacion() {
		return "B";
	}

	@Override
	public String getDescripcionCondAprobacion() {
		return "hay que sumar 19 puntos entre las tres notas, todas tienen que ser 4 o más, no puede haber más de una que baje de 6";
	}

}
