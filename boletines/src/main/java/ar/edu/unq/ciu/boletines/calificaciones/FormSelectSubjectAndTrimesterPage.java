package ar.edu.unq.ciu.boletines.calificaciones;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.ciu.boletines.dominio.Curso;
import ar.edu.unq.ciu.boletines.dominio.Profesor;

public class FormSelectSubjectAndTrimesterPage extends WebPage {


	private static final long serialVersionUID = 1L;
	private FormSelectSubjectAndTrimesterPageController controller = new FormSelectSubjectAndTrimesterPageController();
	
	
	public FormSelectSubjectAndTrimesterPage(Curso course, Profesor loggedProfessor) {
		super();
		this.controller.setLoggedProfessor(loggedProfessor);
		this.controller.setChosenCourse(course);
		this.controller.setOnlyProfessorSubjects();
		this.showDataCourse();
		this.add(this.buildForm());
		   
	    }

	public void showDataCourse() {
		
		this.add(new Label("anio", new PropertyModel<>(this.controller, "chosenCourse.anio")));
		this.add(new Label("division", new PropertyModel<>(this.controller, "chosenCourse.division")));
		this.add(new Label("turno", new PropertyModel<>(this.controller, "chosenCourse.turno")));	
		
		
		this.add(new Label("nombre", new PropertyModel<>(this.controller, "loggedProfessor.nombre")));
		this.add(new Label("apellido", new PropertyModel<>(this.controller, "loggedProfessor.apellido")));
		
	}
	
	
	public Form<FormSelectSubjectAndTrimesterPageController> buildForm(){
    	
    	Form<FormSelectSubjectAndTrimesterPageController> newForm = new Form<FormSelectSubjectAndTrimesterPageController>("formCourse") {
    		
			private static final long serialVersionUID = 1L;

			  protected void onSubmit() {
				 this.setResponsePage(new StudentsQualifyPage(
						 FormSelectSubjectAndTrimesterPage.this.controller.allStudents(),
						 FormSelectSubjectAndTrimesterPage.this.controller.getChosenSubject(),
						 FormSelectSubjectAndTrimesterPage.this.controller.getChosenTrimester(),FormSelectSubjectAndTrimesterPage.this.controller.getLoggedProfessor()));
				        
			  }	 		 
        };
    		
    	newForm.add(new DropDownChoice<>("trimesterSelect",
    			   new PropertyModel<>(this.controller,"chosenTrimester"),
    			     new PropertyModel<>(this.controller, "trimesters")));
    	
    	newForm.add(new DropDownChoice<>("subjectSelect",
 			   new PropertyModel<>(this.controller,"chosenSubject"),
 			     new PropertyModel<>(this.controller, "subjectsToChose"),
 			     new ChoiceRenderer<>("nombre")));
    		
		return newForm;
		
	}	
	
}
