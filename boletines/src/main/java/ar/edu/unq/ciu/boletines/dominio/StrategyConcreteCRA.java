package ar.edu.unq.ciu.boletines.dominio;

import java.io.Serializable;
import java.util.List;

// Strategy concreto para resolver las condiciones para aprobar una materia
// caso 'A': hay que sumar 20 puntos entre las tres notas, y tienen que ser todas 4 o más 
public class StrategyConcreteCRA extends StrategyConditionResolution implements Serializable {

	private static final long serialVersionUID = 5043373120194563602L;

	private static StrategyConcreteCRA instancia;
	
	private StrategyConcreteCRA() {
		super();
	}
	
	public static StrategyConcreteCRA getInstancia() {
		if (StrategyConcreteCRA.instancia == null) {
			StrategyConcreteCRA.instancia = new StrategyConcreteCRA();
		}
		return StrategyConcreteCRA.instancia;
	}

	@Override
	public boolean estaAprobada(List<Integer> notas) {
		return this.tieneTodasLasNotas(notas)
				&& this.sumaDeNotas(notas) >= 20
				&& this.sonTodasLasNotasMayoresOIgualesQue(notas, 4);
	}

	@Override
	public boolean estaDesaprobada(List<Integer> notas) {
		return (this.cuantasNotasTiene(notas) > 0 
					&& !this.sonTodasLasNotasMayoresOIgualesQue(notas, 4))
				|| (this.cuantasNotasTiene(notas) == 2
					&& this.queNotaNecesitaParaAprobar(notas) > 10)
				|| (this.tieneTodasLasNotas(notas)
						&& this.sumaDeNotas(notas) < 20);
	}

	@Override
	public int queNotaNecesitaParaAprobar(List<Integer> notas) {
		return Math.max(4, 20 - this.sumaDeNotas(notas));
	}

	@Override
	public String getNombreCondAprobacion() {
		return "A";
	}

	@Override
	public String getDescripcionCondAprobacion() {
		return "hay que sumar 20 puntos entre las tres notas, y tienen que ser todas 4 o más";
	}

}
