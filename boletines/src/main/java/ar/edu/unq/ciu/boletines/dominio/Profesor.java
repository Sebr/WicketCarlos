package ar.edu.unq.ciu.boletines.dominio;

import java.util.ArrayList;
import java.util.List;

public class Profesor {
	
	private String nombre;
	private String apellido;
	private List<Curso> cursos = new ArrayList<>();
	private List<Materia> materiasQueEnseña = new ArrayList<>();
	private String password;
	private String userName;
	
	public void setMateriasQueEnseña(List<Materia> materiasQueEnseña) {
		this.materiasQueEnseña = materiasQueEnseña;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public List<Materia> getMateriasQueEnseña() {
		return materiasQueEnseña;
	}

	public void agregarCurso(Curso curso) {
		this.cursos.add(curso);
	}
	
	public void agregarMateria(Materia materia) {
		this.materiasQueEnseña.add(materia);
	}
	
	public List<Curso> getCursos() {
		return cursos;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}



	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public Profesor() {
		super();
	}
	
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}


}
