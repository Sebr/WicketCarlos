package ar.edu.unq.ciu.boletines.web.boletines;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import ar.edu.unq.ciu.boletines.dominio.Alumno;
import ar.edu.unq.ciu.boletines.dominio.Curso;
import ar.edu.unq.ciu.boletines.dominio.Materia;
import ar.edu.unq.ciu.boletines.dominio.store.BoletinesStore;

// controller para BoletinPage
public class BoletinPageControlador implements Serializable {

	private static final long serialVersionUID = -7198871174339961065L;

	private BoletinesStore bdas;

	private Alumno alumnoSeleccionado;
	

	// constructor
	public BoletinPageControlador() {
		super();
		this.initStore();
	}

	// getters y setters
	public Alumno getAlumnoSeleccionado() {
		return this.alumnoSeleccionado;
	}

	public void setAlumnoSeleccionado(Alumno alumno) {
		this.alumnoSeleccionado = alumno;
	}

	// queries
	public List<Alumno> getAlumnos() {
		return this.bdas.getAlumnos();
//		return this.bdas.getEstablecimiento().getCursos().stream().flatMap(curso -> curso.getAlumnos().stream())
//				.collect(Collectors.toSet()).stream().collect(Collectors.toList());
	}

	public Alumno getAlumnoPredeterminado() {
		List<Alumno> alumnos = this.getAlumnos();
		if (alumnos.isEmpty()) {
			return null;
		} else {
			return alumnos.get(0);
		}
	}

	public List<MateriaConNotas> getMateriasConNotas() {
		return BoletinPageControlador.getMateriasConNotasDe(this.alumnoSeleccionado);
	}

	public static List<MateriaConNotas> getMateriasConNotasDe(Alumno alumno) {
		Curso curso = alumno.getCurso();
		if (curso == null) {
			return null;
		} else {
			return alumno.getCurso().getMaterias().stream()
					.map(materia -> BoletinPageControlador.mkMateriaConNotas(materia, alumno))
					.collect(Collectors.toList());
		}
	}

	// inicializar una materia con notas
	static MateriaConNotas mkMateriaConNotas(Materia materia, Alumno alumno) {
		MateriaConNotas mcn = new MateriaConNotas(materia,
				alumno.getNotaDeMateria(materia, "1"), // nota 1er. trimestre
				alumno.getNotaDeMateria(materia, "2"), // nota 2do. trimestre
				alumno.getNotaDeMateria(materia, "3")); // nota 3er. trimestre
		if (mcn.estaAprobada()) {
			mcn.setCondicion("aprobada");
		} else if (mcn.estaDesaprobada()) {
			mcn.setCondicion("desaprobada");
		} else if (mcn.leFaltaUnaNota()) {
			mcn.setCondicion("necesita: " + Integer.toString(mcn.queNotaNecesitaParaAprobar()));
		} else {
			mcn.setCondicion("");
		}
		return mcn;
	}

	// inicializar el store
	private void initStore() {
		this.bdas = BoletinesStore.getInstancia();
		this.bdas.cargarDatosPrueba();
	}

}
