package ar.edu.unq.ciu.boletines.altasProfesores;

import org.apache.wicket.markup.html.WebPage;

public class AddProfessorPage extends WebPage{

	
	   private static final long serialVersionUID = 1L;
       private AddProfessorPageController controller = new AddProfessorPageController();
       private SuccessAddedProffesorPanel sapp = new SuccessAddedProffesorPanel("successPanel");
       private  SubjectPanel sp =  new SubjectPanel("subjectPanel",sapp);
       private  CoursePanel cp =  new CoursePanel("coursePanel", this.controller,sp);
      ;
	
	public AddProfessorPage() {	
		super();
		this.add(cp);
		this.sp.setVisible(false);
		this.add(sp);
		this.add(sapp);
		
	}
	
	
	
}