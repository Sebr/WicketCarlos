package ar.edu.unq.ciu.boletines.web.cursos;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;

public class AddCurso extends WebPage {
	private static final long serialVersionUID = -7607228896939415658L;

	
	CursAddController controller;

	public AddCurso(CursAddController _controller) { 
		this.controller = _controller;
		this.add(this.buildForm());
	}
	
	public ChoiceRenderer render = new ChoiceRenderer("nombre");
	

	public Form<CursAddController> buildForm() {
		Form<CursAddController> coursMainDataForm = new Form<CursAddController>("addCours") {
			private static final long serialVersionUID = -7380319985557184605L;
			
			
			protected void onSubmit() {
				AddCurso.this.controller.doAddCurso() ;
				this.setResponsePage(CursoPage.class);
			}
		};

		coursMainDataForm.add(new NumberTextField<Integer>("year", new PropertyModel<>(this.controller, "year")));
		coursMainDataForm.add(new TextField<String>("div", new PropertyModel<>(this.controller, "div")));
		coursMainDataForm.add(new TextField<String>("tourn", new PropertyModel<>(this.controller, "tourn")));
		coursMainDataForm.add(new TextField<String>("precep", new PropertyModel<>(this.controller, "precep")));
		coursMainDataForm.add(new CheckBoxMultipleChoice("checkMat", new PropertyModel<>(this.controller, "mater"), new PropertyModel<>(this.controller, "materiasEstablecimiento"), render));

		return coursMainDataForm;
	}
}



