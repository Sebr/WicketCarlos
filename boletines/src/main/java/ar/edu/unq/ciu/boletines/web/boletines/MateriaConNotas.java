package ar.edu.unq.ciu.boletines.web.boletines;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ar.edu.unq.ciu.boletines.dominio.Materia;

// materia + 3 notas + condición
public class MateriaConNotas implements Serializable {

	private static final long serialVersionUID = 242793570840419972L;

	private Materia materia;
	private List<Integer> notas;
	private String condicion;

	// constructor
	public MateriaConNotas(Materia materia, Integer nota1, Integer nota2, Integer nota3) {
		super();
		this.materia = materia;
		this.notas = new ArrayList<>(Arrays.asList(nota1, nota2, nota3));
	}

	// getters y setters
	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}
	
	public List<Integer> getNotas() {
		return this.notas;
	}

	public String getCondicion() {
		return condicion;
	}

	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	
	public Integer getNota(int trimestre) {
		return this.notas.get(trimestre - 1);
	}
	
	public void setNota(int trimestre, Integer nota) {
		this.notas.set(trimestre - 1, nota);
	}
	
	public Integer getNota1() {
		return this.getNota(1);
	}

	public Integer getNota2() {
		return this.getNota(2);
	}

	public Integer getNota3() {
		return this.getNota(3);
	}

	// queries
	public boolean estaAprobada() {
		return this.materia.estaAprobada(this.notas);
	}
	
	public boolean estaDesaprobada() {
		return this.materia.estaDesaprobada(this.notas);
	}

	public boolean leFaltaUnaNota() {
		return this.materia.leFaltaUnaNota(this.notas);
	}
	
	public int queNotaNecesitaParaAprobar() {
		return this.materia.queNotaNecesitaParaAprobar(this.notas);
	}

}
