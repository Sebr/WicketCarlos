package ar.edu.unq.ciu.boletines.web.api;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.wicketstuff.rest.annotations.MethodMapping;
import org.wicketstuff.rest.resource.gson.GsonRestResource;

import ar.edu.unq.ciu.boletines.dominio.Alumno;
import ar.edu.unq.ciu.boletines.dominio.store.BoletinesStore;
import ar.edu.unq.ciu.boletines.web.boletines.BoletinPageControlador;

public class BoletinesRestResourse extends GsonRestResource {
	
	private static final long serialVersionUID = -6628423514356008438L;

	private BoletinesStore bdas = BoletinesStore.getInstancia();
	
	@MethodMapping("/alumnos")
	public List<AlumnoDataObject> getAlumnos() {
		return this.bdas.getAlumnos().stream()
				.sorted(Comparator.comparing(alumno -> alumno.getNombreYApellido()))
				.map(alumno -> new AlumnoDataObject(alumno))
				.collect(Collectors.toList());
	}
	
	@MethodMapping("/alumno/{dni}")
	public List<MateriaConNotasDataObject> getBoletin(String dni) {
		Optional<Alumno> mbAlumno = this.bdas.getAlumnoPorDNI(dni);
		if (!mbAlumno.isPresent()) {
			return null;
		}
		Alumno alumno = mbAlumno.get();
		if (alumno.getCurso() == null) {
			return null;
		}
		return BoletinPageControlador.getMateriasConNotasDe(alumno).stream()
				.map(mcn -> new MateriaConNotasDataObject(mcn))
				.collect(Collectors.toList());
	}

}
