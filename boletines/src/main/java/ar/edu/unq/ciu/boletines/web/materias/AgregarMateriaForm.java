package ar.edu.unq.ciu.boletines.web.materias;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.ciu.boletines.dominio.Materia;
import ar.edu.unq.ciu.boletines.dominio.StrategyConditionResolution;

// form para cargar datos y realizar el alta de una materia
public class AgregarMateriaForm extends WebPage {

	private static final long serialVersionUID = 8048809456884665525L;

	private MateriasPageControlador ctrl;

	// constructor
	public AgregarMateriaForm(MateriasPageControlador ctrl) {
		super();
		this.ctrl = ctrl;
		
		this.add(this.buildForm());
	}

	// carga de widgets
	private Form<Materia> buildForm() {
		Form<Materia> fm = new Form<Materia>("formMateria") {
			private static final long serialVersionUID = -6519241799962267568L;

			@Override
			protected void onSubmit() {
				AgregarMateriaForm.this.ctrl.agregarMateria();
				this.setResponsePage(new MateriasPage(AgregarMateriaForm.this.ctrl));
			}
		};
		
		fm.add(new TextField<String>("materia", new PropertyModel<>(this.ctrl, "nombreDeMateria"))
				.setRequired(true));
		
		fm.add(new DropDownChoice<StrategyConditionResolution>("condicion",
				new PropertyModel<>(this.ctrl, "condicionDeAprobacion"),
				new PropertyModel<>(this.ctrl, "condiciones"),
				new ChoiceRenderer<>("nombreCondAprobacion")) {
			private static final long serialVersionUID = -8085831226913358625L;

			@Override
			protected String getNullKeyDisplayValue() {
				return "(elegir una)";
				// texto que se muestra cuando no hay condición seleccionada
			}

			@Override
			protected boolean wantOnSelectionChangedNotifications() {
				return true;
				// cuando se selecciona un item, actualizar model object y recargar el form
			}
		}.setRequired(true));
		
		fm.add(new Label("condicionDesc", new PropertyModel<>(this.ctrl, "condicionDeAprobacion.descripcionCondAprobacion")));
		
		return fm;
	}

}
