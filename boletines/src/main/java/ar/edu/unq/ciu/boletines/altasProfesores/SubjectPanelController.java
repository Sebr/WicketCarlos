package ar.edu.unq.ciu.boletines.altasProfesores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unq.ciu.boletines.ProfesorStore.StoreProf;
import ar.edu.unq.ciu.boletines.dominio.Curso;
import ar.edu.unq.ciu.boletines.dominio.Materia;
import ar.edu.unq.ciu.boletines.dominio.Profesor;

public class SubjectPanelController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private String apellido;
	private String password;
	private String userName;
	private StoreProf store = StoreProf.getInstancia(); 
	private List<Curso> chosenCourses = new ArrayList<>();
    private List<Materia> chosenSubjects;
    private List<Materia> subjetsToChoice = new ArrayList<>();
    
    
	public List<Materia> getMateriasParaElegir() {
		return subjetsToChoice;
	}
	public void setMateriasParaElegir(List<Materia> stc) {
		this.subjetsToChoice = stc;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public StoreProf getStore() {
		return store;
	}
	public void setStore(StoreProf store) {
		this.store = store;
	}
	public List<Curso> getChosenCourses() {
		return chosenCourses;
	}
	public void setChosenCourses(List<Curso> cc) {
		this.chosenCourses = cc;
	}
	public List<Materia> getChosenSubjects() {
		return chosenSubjects;
	}
	public void setChosenSubjects(List<Materia> cs) {
		this.chosenSubjects = cs;
	}	
    
	
	public void agregarDocente() {
		Profesor pro = new Profesor();
		pro.setApellido(this.getApellido());
		pro.setNombre(this.getNombre());
		pro.setMateriasQueEnseña(this.getChosenSubjects());
		pro.setPassword(this.getPassword());
		pro.setUserName(this.getUserName());
		pro.setCursos(this.getChosenCourses());
		this.store.agregarProfesor(pro);
	}

}
