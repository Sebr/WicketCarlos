package ar.edu.unq.ciu.boletines.dominio;

import java.io.Serializable;
import java.util.List;

//Strategy concreto para resolver las condiciones para aprobar una materia
//caso 'C': todas las notas tienen que ser 4 o más, y además, o bien tienen que sumar 20, o bien tiene que haber al menos un 9 
public class StrategyConcreteCRC extends StrategyConditionResolution implements Serializable {

	private static final long serialVersionUID = -4187667266029635943L;

	private static StrategyConcreteCRC instancia;
	
	private StrategyConcreteCRC() {
		super();
	}
	
	public static StrategyConcreteCRC getInstancia() {
		if (StrategyConcreteCRC.instancia == null) {
			StrategyConcreteCRC.instancia = new StrategyConcreteCRC();
		}
		return StrategyConcreteCRC.instancia;
	}
	
	@Override
	public boolean estaAprobada(List<Integer> notas) {
		return this.tieneTodasLasNotas(notas)
				&& this.sonTodasLasNotasMayoresOIgualesQue(notas, 4)
				&& (this.sumaDeNotas(notas) >= 20
						|| this.cantNotasIgualesA(notas, 9) >= 1);
	}

	@Override
	public boolean estaDesaprobada(List<Integer> notas) {
		return (this.cuantasNotasTiene(notas) > 0
					&& !this.sonTodasLasNotasMayoresOIgualesQue(notas, 4))
				|| (this.tieneTodasLasNotas(notas)
						&& this.sumaDeNotas(notas) < 20
						&& this.cantNotasIgualesA(notas, 9) < 1)
				|| (this.cuantasNotasTiene(notas) == 2
					&& this.queNotaNecesitaParaAprobar(notas) > 10);
	}

	@Override
	public int queNotaNecesitaParaAprobar(List<Integer> notas) {
		int nota;
		if (this.cantNotasIgualesA(notas, 9) >= 1) {
			nota = 4;
		} else {
			nota = Math.max(4, 20 - sumaDeNotas(notas));
		}
		return nota;
	}

	public long cantNotasIgualesA(List<Integer> notas, int nota_req) {
		return notas.stream()
				.filter(nota -> ((nota != null) && (nota == nota_req)))
				.count();
	}

	@Override
	public String getNombreCondAprobacion() {
		return "C";
	}

	@Override
	public String getDescripcionCondAprobacion() {
		return "todas las notas tienen que ser 4 o más, y además, o bien tienen que sumar 20, o bien tiene que haber al menos un 9";
	}

}
