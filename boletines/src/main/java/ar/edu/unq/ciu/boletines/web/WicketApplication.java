package ar.edu.unq.ciu.boletines.web;

import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.request.resource.ResourceReference;

import ar.edu.unq.ciu.boletines.web.api.BoletinesRestResourse;
import ar.edu.unq.ciu.boletines.web.api.SebaRestResource;

/**
 * Application object for your web application. If you want to run this
 * application without deploying, run the Start class.
 * 
 * @see ar.edu.unq.ciu.Start#main(String[])
 */
public class WicketApplication extends WebApplication {
	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends WebPage> getHomePage() {
		return HomePage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init() {
		super.init();
		// api
		
		this.mountResource("/api2",  new ResourceReference("restReference2"){
			private static final long serialVersionUID = 1L;
			SebaRestResource resource2 = new SebaRestResource();

			@Override
			public IResource getResource() {
				return resource2;
			}
		});
		
		
		
		this.mountResource("/api", new ResourceReference("restReference") {
			private static final long serialVersionUID = -2159920316095811078L;
			
			BoletinesRestResourse resource = new BoletinesRestResourse();
			
			
		
			
					
			@Override
			public IResource getResource() {
				return resource;
				
			}
			
			
			
		});
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		// para corregir/unificar codificación de las páginas - (c) Ramiro
		getRequestCycleSettings().setResponseRequestEncoding("UTF-8");
		getMarkupSettings().setDefaultMarkupEncoding("UTF-8");
	}
	
	/*
	 * configuración para deployment 
	 * @see org.apache.wicket.protocol.http.WebApplication#getConfigurationType()
	 */
//	@Override
//	public RuntimeConfigurationType getConfigurationType() {
//	  return RuntimeConfigurationType.DEPLOYMENT;
//	}

}
