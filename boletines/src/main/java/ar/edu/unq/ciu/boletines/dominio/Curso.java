package ar.edu.unq.ciu.boletines.dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Curso implements Serializable {

	private static final long serialVersionUID = -4382049359028209214L;

	static private int idGenerator =1;

	// atributos
	private List<Materia> materias = new ArrayList<>();
	private List<Alumno> alumnos = new ArrayList<>();
	private String preceptor;
	private int anio;
	private String division;
	private String turno;
	private String profesor;
	private int id;
	
	
	public int getId() {
		return id;
	}

	public String getProfesor() {
		return profesor;
	}

	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}
	
	// constructores
	public Curso() {
		super();
		
	}
	
	
	public void setMaterias(List<Materia> materias) {
		this.materias = materias;
	}
	
	
	public Curso(String preceptor, int anio, String division, String turno) {
		super();
		this.id = idGenerator++; 
		this.anio = anio;
		this.division = division;
		this.turno = turno;
		this.preceptor = preceptor;

	}

	// getters y setters
	public String getPreceptor() {
		return preceptor;
	}
	
	public void setPreceptor(String preceptor) {
		this.preceptor = preceptor;
	}
	
	public int getAnio() {
		return anio;
	}
	
	public void setAnio(int anio) {
		this.anio = anio;
	}
	
	public String getDivision() {
		return division;
	}
	
	public void setDivision(String division) {
		this.division = division;
	}
	
	public String getTurno() {
		return turno;
	}
	
	public void setTurno(String turno) {
		this.turno = turno;
	}
	
	public List<Alumno> getAlumnos() {
		return this.alumnos;
	}

	public List<Materia> getMaterias() {
		return this.materias;
	}
	
	public String getNombre() {
		return Integer.toString(this.anio) + "º " + this.division + " " + this.turno;
	}

	// altas
	public void agregarAlumno(Alumno alumno) {
		this.alumnos.add(alumno);
	}
	
	public void agregarMateria(Materia materia) {
		this.materias.add(materia);	
	}
	
	public String getAnioDivisionYTurno() {
		return this.anio + " " + this.division + " " + this.turno ;
		
	}
	
	
}
