package ar.edu.unq.ciu.boletines.web.api;

import java.util.List;
import java.util.stream.Collectors;

import org.wicketstuff.rest.annotations.MethodMapping;
import org.wicketstuff.rest.resource.gson.GsonRestResource;

import ar.edu.unq.ciu.boletines.dominio.Alumno;
import ar.edu.unq.ciu.boletines.dominio.store.BoletinesStore;


public class SebaRestResource extends GsonRestResource {

	private static final long serialVersionUID = -6282485580025692914L;
	private BoletinesStore bdas = BoletinesStore.getInstancia();
	
	
	/*
	 * 
	 * Primera version desprolija de la api
	
//	@MethodMapping("/cursoEspecifico/{idCurso}/materia/{nombreMateria}/trimestre/{nroTrimestre}")
//	public List<AlumnoConNotaDataObject> getPlanilla3(int idCurso, String nombreDeLaMateria, String trimestre) {	      
//		List<Alumno> alumnosConMateriaElegida = this.bdas.getCursos().stream()
//				.filter(c -> c.getId() == idCurso).collect(Collectors.toList()).get(0).getAlumnos().
//				     stream().filter(a -> a.existeNota(nombreDeLaMateria)).collect(Collectors.toList());
//				     
//		   return alumnosConMateriaElegida.stream().
//				   map(a -> new AlumnoConNotaDataObject(a.getNombreYApellido(), a.getObjetoNotaDeMateria(nombreDeLaMateria, trimestre))).
//				     collect(Collectors.toList());     						
//	}

*/
	
	
	/////Segunda version pulida
	
	@MethodMapping("/cursoEspecifico/{idCurso}/materia/{nombreMateria}/trimestre/{nroTrimestre}")
	public List<AlumnoConNotaDataObject> getPlanilla(int idCurso, String nombreDeLaMateria, String trimestre) {	      
		List<Alumno> alumnosConMateriaElegida = this.bdas.todosLosAlumnosDeUnCursoQueCursanMateria(nombreDeLaMateria, idCurso);
			     
		   return alumnosConMateriaElegida.stream().
				   map(a -> a.alumnoDataObject(nombreDeLaMateria, trimestre)).
				              collect(Collectors.toList());     						
	}

	
	@MethodMapping("/cursos")
	public List<CursoDataObject> getCursos() {	
		return this.bdas.getCursos().stream().map(c -> new CursoDataObject(c)).collect(Collectors.toList());
	}
	
	
	
}
