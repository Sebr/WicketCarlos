package ar.edu.unq.ciu.boletines.web.cursos;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.ciu.boletines.dominio.*;
import ar.edu.unq.ciu.boletines.dominio.store.BoletinesStore;


public class CursoPage extends WebPage  {

	private static final long serialVersionUID = 1149110168970101051L;
	CursAddController controller;
	
	public CursoPage(){
		super();
		this.controller = new CursAddController();
		this.fillCoursTable();
		this.fillAddCoursButton();
		
	}

	protected void fillAddCoursButton() {
		Link<String> addCoursAction = new Link<String>("addCurso") {
			private static final long serialVersionUID = -901862819067967823L;

			@Override
			public void onClick() {
				this.setResponsePage(new AddCurso(CursoPage.this.controller));
				
			}
		}; 
		this.add(addCoursAction);
	}

	protected void fillCoursTable() {
		ListView<Curso> listTable = new ListView<Curso>
		("courses", new PropertyModel<>(this, "courses")) {
			private static final long serialVersionUID = 2471303321870848346L;

			@Override
			protected void populateItem(ListItem<Curso> panel) {
				CompoundPropertyModel<Curso> coursModel = 
						new CompoundPropertyModel<>(panel.getModelObject());
				panel.add(new Label("year", coursModel.bind("anio")));
				panel.add(new Label("div", coursModel.bind("division")));
				panel.add(new Label("turn", coursModel.bind("turno")));			
					}
				}; 
				
		this.add(listTable);
	}
			
	public List<Curso> getCourses() {
		return BoletinesStore.getInstancia().getCursos().stream()
				.collect(Collectors.toList());
	}
}
		
