package ar.edu.unq.ciu.boletines.web.cursos;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ar.edu.unq.ciu.boletines.dominio.Curso;
import ar.edu.unq.ciu.boletines.dominio.Materia;
import ar.edu.unq.ciu.boletines.dominio.store.BoletinesStore;



public class CursAddController implements Serializable {
	private static final long serialVersionUID = 7230995521136850909L;
	
	public String div;
	public int year;
	public String tourn;
	public String precep;
	public List<Materia> mater = new ArrayList<>();

	
	
	public List<Materia> getMater() {
		return mater;
	}


	public void setMater(List<Materia> mater) {
		this.mater = mater;
	}


	public CursAddController(){
		super();
	}

	
	public CursAddController(String precep, int year, String div, String tourn) {
		this.div = div;
		this.precep = precep;
		this.year = year;
		this.tourn = tourn;
	}
	
	public Collection<Materia> getMateriasEstablecimiento(){
		return BoletinesStore.getInstancia().getMaterias();
	}
	
	public void setMateriasCurso(List<Materia> mater){
		this.mater = mater;
	}
	

	public List<Materia> getMaterCurso() {
		return mater;
	}


	public String getPrecep() { return this.precep; }
	public void setPrecep(String _name) { this.precep = _name; }
	
	
	public int getYear() { return this.year; }
	public void setYear(int _year) { this.year = _year; }
	
	public String getDiv() { return this.div; }
	public void setDiv(String div) { this.div = div; }
	
	public String getTourn() { return this.tourn; }
	public void setTourn(String tourn) { this.tourn = tourn; }

	

	public void doAddCurso() {
		Curso newCurso = new Curso(this.getPrecep(), this.getYear(), this.getDiv(), this.getTourn());
		newCurso.setMaterias(this.getMater());
		BoletinesStore.getInstancia().getEstablecimiento().addCurso(newCurso);				
	}




	
}
