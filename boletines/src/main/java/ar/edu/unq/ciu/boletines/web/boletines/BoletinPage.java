package ar.edu.unq.ciu.boletines.web.boletines;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.ciu.boletines.dominio.Alumno;

// página para ver el boletín de un alumno seleccionado
public class BoletinPage extends WebPage {

	private static final long serialVersionUID = -2251717835373545299L;

	private BoletinPageControlador ctrl;

	// constructores
	public BoletinPage() {
		super();

		this.ctrl = new BoletinPageControlador();
		this.ctrl.setAlumnoSeleccionado(this.ctrl.getAlumnoPredeterminado());

		this.configurarPagina();
	}

	public BoletinPage(BoletinPageControlador ctrl) {
		super();

		this.ctrl = ctrl;

		this.configurarPagina();
	}
	
	// carga de widgets
	private void configurarPagina() {
		this.add(new Label("alumno", new PropertyModel<>(this.ctrl, "alumnoSeleccionado.nombreYApellido")));

		this.add(new Label("anio", new PropertyModel<>(this.ctrl, "alumnoSeleccionado.curso.anio")));
		this.add(new Label("division", new PropertyModel<>(this.ctrl, "alumnoSeleccionado.curso.division")));
		this.add(new Label("turno", new PropertyModel<>(this.ctrl, "alumnoSeleccionado.curso.turno")));

		this.add(new ListView<MateriaConNotas>("materias", new PropertyModel<>(this.ctrl, "materiasConNotas")) {
			private static final long serialVersionUID = -3149006582427608486L;

			@Override
			protected void populateItem(ListItem<MateriaConNotas> item) {
				MateriaConNotas materia = item.getModelObject();

				item.add(new Label("materia", new PropertyModel<>(materia, "materia.nombre")));
				item.add(new Label("trimestre1", new PropertyModel<>(materia, "nota1")));
				item.add(new Label("trimestre2", new PropertyModel<>(materia, "nota2")));
				item.add(new Label("trimestre3", new PropertyModel<>(materia, "nota3")));
				item.add(new Label("condicion", new PropertyModel<>(materia, "condicion")));
			}
		});

		PropertyModel<Alumno> pmalumno = new PropertyModel<>(this.ctrl, "alumnoSeleccionado");

		Form<Alumno> falumno = new Form<Alumno>("formAlumno") {
			private static final long serialVersionUID = -6477961500999708646L;

			@Override
			protected void onSubmit() {
				BoletinPage.this.ctrl.setAlumnoSeleccionado(pmalumno.getObject());
				this.setResponsePage(new BoletinPage(BoletinPage.this.ctrl));
			}
		};

		falumno.add(new DropDownChoice<Alumno>("alumnos", pmalumno, new PropertyModel<>(this.ctrl, "alumnos"),
				new ChoiceRenderer<>("nombreYApellido")) {
			private static final long serialVersionUID = -3276629044844712486L;

			@Override
			protected boolean wantOnSelectionChangedNotifications() {
				return true;
				// cuando se selecciona un item, actualizar model object y recargar el form
			}
		});
		this.add(falumno);
	}

}
