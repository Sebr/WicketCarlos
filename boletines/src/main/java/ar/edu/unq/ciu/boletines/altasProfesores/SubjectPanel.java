package ar.edu.unq.ciu.boletines.altasProfesores;

import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.ListMultipleChoice;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.ciu.boletines.dominio.Materia;

public class SubjectPanel  extends Panel{
	
	SuccessAddedProffesorPanel sapp;
	
    private static final long serialVersionUID = 1L;
    
	 private SubjectPanelController controller = new SubjectPanelController();

	
	public SubjectPanel(String id, SuccessAddedProffesorPanel panel) {
		super(id);
		this.sapp = panel;
		this.add(this.buildForm());
	
	}

	public SubjectPanelController getController() {
		return controller;
	}

	public Form<SubjectPanelController> buildForm(){
		
		Form<SubjectPanelController> newForm = new Form<SubjectPanelController>("subjectForm") {
			
			private static final long serialVersionUID = 1L;
			
			 protected void onSubmit() {
			
				 
				 SubjectPanel.this.controller.agregarDocente();
				 SubjectPanel.this.setVisible(false);
				 SubjectPanel.this.sapp.setVisible(true);
				 
			
			  }		
			
		};
		
		ListMultipleChoice<Materia> subList = new ListMultipleChoice<Materia>("listOfSubjectsId",
				new PropertyModel<>(this.controller,"chosenSubjects") ,
				  new PropertyModel<>(this.controller, "subjetsToChoice"),
				   new ChoiceRenderer<>("nombre"));
	
		subList.setRequired(true);
		newForm.add(subList);
		return newForm;
		
	}
	
	
	
	
	
	
	
	

}
