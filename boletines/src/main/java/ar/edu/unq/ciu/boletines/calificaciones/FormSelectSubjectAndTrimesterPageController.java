package ar.edu.unq.ciu.boletines.calificaciones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ar.edu.unq.ciu.boletines.dominio.Alumno;
import ar.edu.unq.ciu.boletines.dominio.Curso;
import ar.edu.unq.ciu.boletines.dominio.Materia;
import ar.edu.unq.ciu.boletines.dominio.Profesor;

public class FormSelectSubjectAndTrimesterPageController implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private List<String> trimesters = new ArrayList<>();
	private String chosenTrimester;
	private Curso chosenCourse; 
	private Materia chosenSubject;
	private Profesor loggedProfessor;
	private List<Materia> subjectsToChose = new ArrayList<>();
	
	

	public List<Materia> getSubjectsToChose() {
		return subjectsToChose;
	}
	public void setSubjectsToChose(List<Materia> cs) {
		this.subjectsToChose = cs;
	}
	public Profesor getLoggedProfessor() {
		return loggedProfessor;
	}
	public void setLoggedProfessor(Profesor lp) {
		this.loggedProfessor = lp;
	}
	public Materia getChosenSubject() {
		return chosenSubject;
	}
	public void setChosenSubject(Materia cs) {
		this.chosenSubject = cs;
	}
	public void setChosenCourse(Curso cc) {
		this.chosenCourse = cc;
	}
	public Curso getChosenCourse() {
		return chosenCourse;
	}
	public String getChosenTrimester() {
		return chosenTrimester;
	}
	public void setChosenTrimester(String ct) {
		this.chosenTrimester = ct;
	}

    public List<Alumno> allStudents(){
    	return this.chosenCourse.getAlumnos();
    }
      
	   
	// constructor
	   public FormSelectSubjectAndTrimesterPageController(){
			super();
			this.trimesters.add("1");
	    	this.trimesters.add("2");
	    	this.trimesters.add("3");
	 		
	   }
	   
	   public void setOnlyProfessorSubjects(){ 
		   this.setSubjectsToChose( this.chosenCourse.getMaterias().stream().
			   filter(m -> this.getLoggedProfessor().getMateriasQueEnseña().contains(m)).collect(Collectors.toList()))  ;
	   }
}
