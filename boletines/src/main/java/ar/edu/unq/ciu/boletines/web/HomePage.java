package ar.edu.unq.ciu.boletines.web;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

import ar.edu.unq.ciu.boletines.logIn.LoginPage;
import ar.edu.unq.ciu.boletines.web.alumnos.AlumnoPage;
import ar.edu.unq.ciu.boletines.web.boletines.BoletinPage;
import ar.edu.unq.ciu.boletines.web.cursos.CursoPage;
import ar.edu.unq.ciu.boletines.web.materias.MateriasPage;

public class HomePage extends WebPage {

	private static final long serialVersionUID = 9119801710564155559L;

	public HomePage() {
		super();

		this.configurarPagina();
	}

	private void configurarPagina() {
		
		this.add(new Link<String>("alumnos") {
			private static final long serialVersionUID = -8809899404268408515L;

			@Override
			public void onClick() {
				this.setResponsePage(AlumnoPage.class);
			}
		});

		this.add(new Link<Void>("boletines") {
			private static final long serialVersionUID = -6368974667351053780L;

			@Override
			public void onClick() {
				this.setResponsePage(BoletinPage.class);
			}
		});
		
		this.add(new Link<String>("cursos") {
			private static final long serialVersionUID = -5714398010396372891L;

			@Override
			public void onClick() {
				this.setResponsePage(CursoPage.class);
			}
		});
		
		
	
		
		this.add(new Link<String>("calificaciones") {
			private static final long serialVersionUID = -5714398010396372291L;

			@Override
			public void onClick() {
				this.setResponsePage(LoginPage.class);
			}
		});
		
		
		this.add(new Link<Void>("materias") {
			private static final long serialVersionUID = -8809899404268408515L;

			@Override
			public void onClick() {
				this.setResponsePage(MateriasPage.class);
			}
		});
		
		
	}

}
