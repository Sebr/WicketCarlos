package ar.edu.unq.ciu.boletines.calificaciones;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.ciu.boletines.dominio.Profesor;

public class FormSelectCoursePage extends WebPage{

	private static final long serialVersionUID = 1L;
	private FormSelectCoursePageController controller = new FormSelectCoursePageController();
	
	public FormSelectCoursePage(Profesor loggedProf) {
		super();
		this.controller.setLoggedProfessor(loggedProf);
		this.controller.setCoursesToChose(loggedProf.getCursos());
		this.add(this.buildForm());
		
	}
	
	public Form<FormSelectCoursePageController> buildForm(){
	    	
	    	Form<FormSelectCoursePageController> newForm = new Form<FormSelectCoursePageController>("formCourse") {
	    		
				private static final long serialVersionUID = 1L;

				  protected void onSubmit() {
					  this.setResponsePage
					  (new FormSelectSubjectAndTrimesterPage
					    (FormSelectCoursePage.this.controller.getChosenCourse(),FormSelectCoursePage.this.controller.getLoggedProfessor()));
				  }	 		 
	        };
	    		
	    	newForm.add(new DropDownChoice<>("CourseSelectId",
	    			   new PropertyModel<>(this.controller,"chosenCourse"),
	    			     new PropertyModel<>(this.controller, "coursesToChose"),
	    			      new ChoiceRenderer<>("anioDivisionYTurno")));
	    		
			return newForm;
			
		}
	    	
}
