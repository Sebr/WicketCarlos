package ar.edu.unq.ciu.boletines.web.api;

import ar.edu.unq.ciu.boletines.dominio.Alumno;
import ar.edu.unq.ciu.boletines.dominio.Curso;

public class AlumnoDataObject {

	private String dni;
	private String nombreYApellido;
	private String curso;

	// constructor
	
	public AlumnoDataObject(Alumno alumno) {
		super();
		this.dni = alumno.getDni();
		this.nombreYApellido = alumno.getNombreYApellido();
		
		Curso curso = alumno.getCurso();
		if (curso != null) {
			this.curso = "año: " + curso.getAnio() 
						+ "º - división: " + curso.getDivision()
						+ " - turno: " + curso.getTurno();
		}
	}

	// getters

	public String getDni() {
		return this.dni;
	}

	public String getNombreYApellido() {
		return this.nombreYApellido;
	}

	public String getCurso() {
		return curso;
	}
	
}
