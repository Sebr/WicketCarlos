package ar.edu.unq.ciu.boletines.altasProfesores;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.ListMultipleChoice;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.ciu.boletines.dominio.Curso;

public class CoursePanel extends Panel{
	
	private static final long serialVersionUID = 1L;
	
	 private AddProfessorPageController controller;
	 private SubjectPanel subjectPanel;

    public CoursePanel(String id, AddProfessorPageController ctr, SubjectPanel sp ) {
    	
		   super(id);
		   this.controller = ctr;
		   this.subjectPanel = sp;
		   this.add(this.buildForm());
		
	}

	public AddProfessorPageController getController() {
		return controller;
	}

	
    public SubjectPanel getSubjectPanel() {
		return subjectPanel;
	}

    public Form<AddProfessorPageController> buildForm(){
		
		Form<AddProfessorPageController> newForm = new Form<AddProfessorPageController>("professorForm") {
			
			private static final long serialVersionUID = 1L;
			
			 protected void onSubmit() {
				 
				 
				 if(CoursePanel.this.controller.passIsCorrect() && !CoursePanel.this.controller.getStore().userAlreadyExists(CoursePanel.this.controller.getUserName(),CoursePanel.this.controller.getPassword())) {
				 	 
				    CoursePanel.this.controller.allSubjectsOfChosenCourses();
				    CoursePanel.this.subjectPanel.getController().setMateriasParaElegir( CoursePanel.this.controller.getSubjectsToChoice());
				    CoursePanel.this.subjectPanel.getController().setApellido(CoursePanel.this.controller.getApellido());
				    CoursePanel.this.subjectPanel.getController().setNombre(CoursePanel.this.controller.getNombre());
				    CoursePanel.this.subjectPanel.getController().setPassword(CoursePanel.this.controller.getPassword());
				    CoursePanel.this.subjectPanel.getController().setUserName(CoursePanel.this.controller.getUserName());
				    CoursePanel.this.subjectPanel.getController().setChosenCourses(CoursePanel.this.controller.getCursosChoice());
				    CoursePanel.this.setVisible(false);
				    CoursePanel.this.subjectPanel.setVisible(true);
				    
				 }else {
					 
					 if(CoursePanel.this.controller.passIsCorrect() && CoursePanel.this.controller.getStore().userAlreadyExists(CoursePanel.this.controller.getUserName(),CoursePanel.this.controller.getPassword())) {
						 CoursePanel.this.controller.setMessage("Este docente ya existe");
					 }else {
					 
					   CoursePanel.this.controller.setMessage("La contraseña y el usuario no coinciden");
					 }
				 }
			  }	
		};
		
		Label newLabel = new Label("message",  new PropertyModel<>(this.controller,"message"));
		TextField<String> newTextField = new TextField<String>("nombreProf", new PropertyModel<>(this.controller, "nombre"));
		TextField<String> newTextField2 = new TextField<String>("apellidoProf", new PropertyModel<>(this.controller, "apellido"));
		TextField<String> newTextField3 = new TextField<String>("usernameProf", new PropertyModel<>(this.controller, "userName"));
		PasswordTextField newPassField = new PasswordTextField("passwordProf", new PropertyModel<>(this.controller, "password"));
	
	    
		newTextField3.setRequired(true);
		newTextField.setRequired(true);
		newTextField2.setRequired(true);
		newPassField.setRequired(true);
		
     	newForm.add(newLabel);
		newForm.add(newTextField);
		newForm.add(newTextField2);
		newForm.add(newPassField);
		newForm.add(newTextField3);
        
		
		ListMultipleChoice<Curso> listCursos = new ListMultipleChoice<Curso>("selectCurso",
				new PropertyModel<>(CoursePanel.this.controller,"cursosChoice") ,
				  new PropertyModel<>(CoursePanel.this.controller,"todosLosCursos"),
				   new ChoiceRenderer<>("anioDivisionYTurno"));
	
		listCursos.setRequired(true);
		newForm.add(listCursos);

				
		return newForm;
		
	}

}
