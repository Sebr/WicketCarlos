package ar.edu.unq.ciu.boletines.calificaciones;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.validator.RangeValidator;

import ar.edu.unq.ciu.boletines.dominio.Alumno;
import ar.edu.unq.ciu.boletines.dominio.Materia;
import ar.edu.unq.ciu.boletines.dominio.Profesor;

public class StudentsQulifyPageController implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	private List<Alumno> studentsToQualify = new ArrayList<>();
	private Profesor loggedProfessor;
	private List<Form<StudentQualifyFormController>> formList = new ArrayList<>();
	private Materia subjectToShow;
	private String trimester;
	

	public void qualifyAll() {
		
		 this.formList.stream().
		  filter(f -> !f.getModelObject().NotaDeMateriaIsAlreadyExists(subjectToShow, trimester)).
		   collect(Collectors.toList()).
		    forEach(f -> f.getModelObject().qualify());

	}
	
	public String getTrimester() {
		return trimester;
	}


	public void setTrimester(String trimester) {
		this.trimester = trimester;
	}


	public Materia getSubjectToShow() {
		return subjectToShow;
	}


	public void setSubjectToShow(Materia subjectToShow) {
		this.subjectToShow = subjectToShow;
	}


	public List<Form<StudentQualifyFormController>> getFormList() {
		return formList;
	}

	 
	public void fillListForms(Materia subject, String trimester) {
			this.getStudentsToQualify().stream().
			  forEach(s -> this.formList.add(this.buildForm(s,subject,trimester)));             	
	}
	 
	 
	public Form<StudentQualifyFormController> buildForm(Alumno student, Materia subject, String trimester){
	
		 StudentQualifyFormController ctr  = new StudentQualifyFormController();
		     ctr.setStudent(student);
		     ctr.setSubject(subject);
		     ctr.setTrimester(trimester);
		     ctr.setCalification(ctr.getStudent().getNotaDeMateria(ctr.getSubject(), ctr.getTrimester()));
		     
	    	Form<StudentQualifyFormController> newForm = new Form<StudentQualifyFormController>("studentsForm", new Model<StudentQualifyFormController>(ctr)) {
	    		
				private static final long serialVersionUID = 1L;

	        };
	
	    	TextField<Integer> newNumberField = new TextField<Integer>("nota", 
	    			new PropertyModel<>(ctr, "calification"));
	    		
	    	newNumberField.setType(Integer.class);
	    	newNumberField.add(RangeValidator.range(1,10));
	    	
	    	
	    	
	    	if(ctr.NotaDeMateriaIsAlreadyExists(ctr.getSubject(), ctr.getTrimester())) {
	    		newNumberField.setEnabled(false);
	        	
	        }
	    	
	    	
	    	newForm.add(new Label("alumno", ctr.getStudent().getNombreYApellido()));
	    	newForm.add(newNumberField);
	    	
    	return newForm;
			
	}
	    	
	
	
	public Profesor getLoggedProfessor() {
		return loggedProfessor;
	}

	public void setLoggedProfessor(Profesor loggedP) {
		this.loggedProfessor = loggedP;
	}

	public List<Alumno> getStudentsToQualify() {
		return studentsToQualify;
	}

	public void setStudentsToQualify(List<Alumno> stq) {
		this.studentsToQualify = stq;
	}

	public StudentsQulifyPageController() {
    
    }
	
}
