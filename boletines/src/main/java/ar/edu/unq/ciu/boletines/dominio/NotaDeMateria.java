package ar.edu.unq.ciu.boletines.dominio;

import java.io.Serializable;

public class NotaDeMateria implements Serializable {
	
	private static final long serialVersionUID = 1779819087805008901L;
	
	// atributos
	private Materia materia;
	private int nota;
	private String trimestre;
	

	// constructores
	public NotaDeMateria(Materia materia, int nota, String trim) {
		super();
		this.materia = materia;
		this.nota = nota;
		this.trimestre = trim;
	}
	
	// getters y setters
	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	public String getTrimestre() {
		return trimestre;
	}

	public void setTrimestre(String timestre) {
		this.trimestre = timestre;
	}
	
	public String nombreMateria(Materia materia) {
		
		return materia.getNombre();
	}

}