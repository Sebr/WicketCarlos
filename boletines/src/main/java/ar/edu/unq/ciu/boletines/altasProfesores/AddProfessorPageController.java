package ar.edu.unq.ciu.boletines.altasProfesores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ar.edu.unq.ciu.boletines.ProfesorStore.StoreProf;
import ar.edu.unq.ciu.boletines.dominio.Curso;
import ar.edu.unq.ciu.boletines.dominio.Materia;
import ar.edu.unq.ciu.boletines.dominio.Profesor;
import ar.edu.unq.ciu.boletines.dominio.store.BoletinesStore;

public class AddProfessorPageController implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private String apellido;
	
	private StoreProf store = StoreProf.getInstancia(); 
	private List<Curso> todosLosCursos = BoletinesStore.getInstancia().getCursos(); 
	private Materia materiaElegida;
	private Curso cursoElegido;
    private List<Materia> chosenSubjects;
    private List<Curso> cursosChoice = new ArrayList<>();
    
    private String password ;
	private String userName;
	
	private List<Materia> subjetsToChoice = new ArrayList<>();
	private String message = " ";
	
   
    
    public String getMessage() {
		return message;
	}


	public void setMessage(String passwordStat) {
		this.message = passwordStat;
	}


	public boolean passIsCorrect() {
    	return this.password.equals(this.userName);
    }
    
    
	public List<Materia> getSubjectsToChoice() {
		return subjetsToChoice;
	}


	public void setSubjectsToChoice(List<Materia> materiasParaElegir) {
		this.subjetsToChoice = materiasParaElegir;
	}


	public void  allSubjectsOfChosenCourses() {
		subjetsToChoice.addAll( this.getCursosChoice().stream().map(c -> c.getMaterias()).
    			flatMap(l -> l.stream()).collect(Collectors.toSet()));
    }
    
    
	public List<Materia> getChosenSubjects() {
		return chosenSubjects;
	}


	public void setChosenSubjects(List<Materia> checkMateria) {
		this.chosenSubjects = checkMateria;
	}

	
	public List<Curso> getCursosChoice() {
		return cursosChoice;
	}


	public void setCursosChoice(List<Curso> cursosChoice) {
		this.cursosChoice = cursosChoice;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}

	
	public List<Curso> getTodosLosCursos() {
		return todosLosCursos;
	}

	
	public Curso getCursoElegido() {
		return cursoElegido;
	}

	public void setCursoElegido(Curso cursoElegido) {
		this.cursoElegido = cursoElegido;
	}

	public Materia getMateriaElegida() {
		return materiaElegida;
	}

	public void setMateriaElegida(Materia materiaElegida) {
		this.materiaElegida = materiaElegida;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public StoreProf getStore() {
		return store;
	}

	public void agregarDocente() {
		Profesor pro = new Profesor();
		pro.setApellido(this.getApellido());
		pro.setNombre(this.getNombre());
		pro.getMateriasQueEnseña().add(this.getMateriaElegida());
		pro.setPassword(this.getPassword());
		pro.setUserName(this.getUserName());
		pro.setCursos(this.getCursosChoice());
		this.store.agregarProfesor(pro);
	}
	
}
