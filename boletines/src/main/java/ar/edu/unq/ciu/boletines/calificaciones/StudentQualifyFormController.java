package ar.edu.unq.ciu.boletines.calificaciones;

import java.io.Serializable;

import ar.edu.unq.ciu.boletines.dominio.Alumno;
import ar.edu.unq.ciu.boletines.dominio.Materia;

public class StudentQualifyFormController implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	
	//Atributos
	
		private Alumno student;
	    private Materia subject;
	    private int calification;
	    private String trimester;
	 
		public Materia getSubject() {
			return subject;
		}

		public void setSubject(Materia su) {
			this.subject = su;
		}

	   
		public String getTrimester() {
			return trimester;
		}

		public void setTrimester(String tr) {
			this.trimester = tr;
		}

	    public int getCalification() {
			return calification;
		}

		public void setCalification(Integer ca) {
			if (ca == null) {
				this.calification = 0;
			}else {
			  this.calification = ca;
			} 
		}


		public Alumno getStudent() {
			return student;
		}

		public void setStudent(Alumno st) {
			this.student = st;
		}
		
	 
	    
	    public StudentQualifyFormController() {
	    	super();   
	    }
	    
	                            
	    public void qualify() {
	    	
	    	this.getStudent().agregarNotaDeMateria(this.getCalification(), this.getSubject(), this.getTrimester());
	    }
	    
	    
	    public boolean NotaDeMateriaIsAlreadyExists(Materia mat, String trimestre) {
	    	return this.getStudent().getNotasDeMaterias().
	    			stream().
	    			   anyMatch(nm -> nm.getMateria().getNombre().
	    				equals(mat.getNombre()) && nm.getTrimestre().equals(trimestre));
	
	
        }
	    
	   
	    
}
