package ar.edu.unq.ciu.boletines.dominio.store;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import ar.edu.unq.ciu.boletines.dominio.Alumno;
import ar.edu.unq.ciu.boletines.dominio.Curso;
import ar.edu.unq.ciu.boletines.dominio.Establecimiento;
import ar.edu.unq.ciu.boletines.dominio.Materia;
import ar.edu.unq.ciu.boletines.dominio.StrategyConcreteCRA;
import ar.edu.unq.ciu.boletines.dominio.StrategyConcreteCRB;
import ar.edu.unq.ciu.boletines.dominio.StrategyConcreteCRC;

public class BoletinesStore implements Serializable {

	private static final long serialVersionUID = 8621941376796862034L;

	private static BoletinesStore instancia;

	private Establecimiento establecimiento;
	
	private Random rnd = new Random();

	// constructores

	private BoletinesStore() {
		super();
	}

	// instancia singleton

	public static BoletinesStore getInstancia() {
		if (instancia == null) {
			instancia = new BoletinesStore();
		}
		return instancia;
	}
	
	// altas a colecciones	
	
	public void addCurso(Curso newCurso) {
		this.establecimiento.addCurso(newCurso);
	}
	
	public void addMateria(Materia materia) {
		this.establecimiento.addMateria(materia);
	}

	public void addAlumno(Alumno newAlumno) {
		this.establecimiento.addAlumno(newAlumno);
	}

	// consultas

	public Establecimiento getEstablecimiento() {
		return this.establecimiento;
	}
	
	public List<Curso> getCursos() {
		this.cargarDatosPrueba();
		return this.establecimiento.getCursos();
	}

	public Collection<Materia> getMaterias() {
		return this.getEstablecimiento().getMaterias();
	}

	public List <Alumno> getAlumnos() {
		this.cargarDatosPrueba();
      	return this.getEstablecimiento().getAlumnos();
	}

	public Optional<Alumno> getAlumnoPorDNI(String dni) {
		return this.establecimiento.getAlumnos().stream()
				.filter(alumno -> alumno.getDni().equals(dni))
				.findAny();
	}
	
	
	public Curso getCurso(int id) {
		return this.getCursos().stream().filter(c -> c.getId() == id).collect(Collectors.toList()).get(0);
	}
	
	
	public List<Alumno> todosLosAlumnosDeUnCurso(int idCurso){
		return this.getCurso(idCurso).getAlumnos();
	}
	
	public List<Alumno> alumnosQueCursanMateria(String nombreDeMateria, int idCurso){
		return this.todosLosAlumnosDeUnCurso(idCurso).stream().filter(a -> a.existeNota(nombreDeMateria)).collect(Collectors.toList());
	}
	 
	public List<Alumno> todosLosAlumnosDeUnCursoQueCursanMateria(String nombreDeMateria,int idCurso){
		return this.alumnosQueCursanMateria(nombreDeMateria, idCurso);
	}
	
	// datos de prueba

	public void cargarDatosPrueba() {
		if (this.establecimiento == null) {
			this.establecimiento = new Establecimiento();
			this.cargarMaterias();
			this.cargarCursos();
		}
	}

	private void cargarMaterias() {
		Materia materia1 = new Materia("Matematica", StrategyConcreteCRA.getInstancia());
		this.establecimiento.addMateria(materia1);

		Materia materia2 = new Materia("Fisica", StrategyConcreteCRB.getInstancia());
		this.establecimiento.addMateria(materia2);

		Materia materia3 = new Materia("Quimica", StrategyConcreteCRC.getInstancia());
		this.establecimiento.addMateria(materia3);

		Materia materia4 = new Materia("Informatica", StrategyConcreteCRA.getInstancia());
		this.establecimiento.addMateria(materia4);
		
		
	}

	private void cargarCursos() {
		Curso curso1oA = new Curso("Héctor Polanco Pradas", 1, "A", "tarde");
		//curso1oA.setId("pija");
		this.agregarMaterias(curso1oA);
		this.agregarAlumnos1oA(curso1oA);
		this.establecimiento.addCurso(curso1oA);

		Curso curso2oA = new Curso("Silvia Coronel", 2, "A", "tarde");
		//curso2oA.setId("culo");
		this.agregarMaterias(curso2oA);
		this.agregarAlumnos2oA(curso2oA);
		this.establecimiento.addCurso(curso2oA);

		Curso curso3oA = new Curso("Ángeles Elizalde", 3, "A", "tarde");
		//curso3oA.setId("caca");
		this.agregarMaterias(curso3oA);
		this.agregarAlumnos3oA(curso3oA);
		this.establecimiento.addCurso(curso3oA);
	}

	private void agregarMaterias(Curso curso) {
		for (Materia materia : this.establecimiento.getMaterias()) {
			curso.agregarMateria(materia);
		}
	}

	private void agregarAlumnos1oA(Curso curso1oA) {
		Alumno alumno1 = new Alumno("45226350", "Cazorla", "Víctor Manuel", "453542", "vmcazorla@establecimiento.edu.ar");
		alumno1.setCurso(curso1oA);
		this.generarNotasAleatorias(alumno1);
		curso1oA.agregarAlumno(alumno1);
		this.establecimiento.addAlumno(alumno1);

		Alumno alumno2 = new Alumno("45151443", "Jurado", "Josefina", "451067", "jjurado@establecimiento.edu.ar");
		alumno2.setCurso(curso1oA);
		this.generarNotasAleatorias(alumno2);
		curso1oA.agregarAlumno(alumno2);
		this.establecimiento.addAlumno(alumno2);

		Alumno alumno3 = new Alumno("45371672", "Merchan", "María Luisa", "456009", "mlmerchan@establecimiento.edu.ar");
		alumno3.setCurso(curso1oA);
		this.generarNotasAleatorias(alumno3);
		curso1oA.agregarAlumno(alumno3);
		this.establecimiento.addAlumno(alumno3);

		Alumno alumno4 = new Alumno("45501228", "Calvinho", "Iván", "452079", "icalvinho@establecimiento.edu.ar");
		alumno4.setCurso(curso1oA);
		this.generarNotasAleatorias(alumno4);
		curso1oA.agregarAlumno(alumno4);
		this.establecimiento.addAlumno(alumno4);
	}

	private void agregarAlumnos2oA(Curso curso2oA) {
		Alumno alumno1 = new Alumno("45941901", "De La Rosa", "Silvia", "454072", "sdelarosa@establecimiento.edu.ar");
		alumno1.setCurso(curso2oA);
		this.generarNotasAleatorias(alumno1);
		curso2oA.agregarAlumno(alumno1);
		this.establecimiento.addAlumno(alumno1);

		Alumno alumno2 = new Alumno("45929434", "Pineda", "Mario", "455521", "mpineda@establecimiento.edu.ar");
		alumno2.setCurso(curso2oA);
		this.generarNotasAleatorias(alumno2);
		curso2oA.agregarAlumno(alumno2);
		this.establecimiento.addAlumno(alumno2);

		Alumno alumno3 = new Alumno("45895976", "Ricart", "María Nieves", "454943", "mnricart@establecimiento.edu.ar");
		alumno3.setCurso(curso2oA);
		this.generarNotasAleatorias(alumno3);
		curso2oA.agregarAlumno(alumno3);
		this.establecimiento.addAlumno(alumno3);
	}

	private void agregarAlumnos3oA(Curso curso3oA) {
		Alumno alumno1 = new Alumno("45823786", "Novoa", "Isabel Pilar", "454565", "ipnovoa@establecimiento.edu.ar");
		alumno1.setCurso(curso3oA);
		this.generarNotasAleatorias(alumno1);
		curso3oA.agregarAlumno(alumno1);
		this.establecimiento.addAlumno(alumno1);

		Alumno alumno2 = new Alumno("45200541", "Serna", "Manuel", "454393", "mserna@establecimiento.edu.ar");
		alumno2.setCurso(curso3oA);
		this.generarNotasAleatorias(alumno2);
		curso3oA.agregarAlumno(alumno2);
		this.establecimiento.addAlumno(alumno2);
	}

	private void generarNotasAleatorias(Alumno alumno) {
		for (Materia materia : alumno.getCurso().getMaterias()) {
			int cant = 2;
			double chance = this.rnd.doubles(1).findFirst().getAsDouble();
			if (chance > 0.5) {
				cant++;
			} else if (chance < 0.25) {
				cant--;
			}
			for (int i = 1; i <= cant; i++) {
				alumno.agregarNotaDeMateria(this.generarNota(), materia, Integer.toString(i));
			}
		}
	}

	private int generarNota() {
		int offset;
		double chance = this.rnd.doubles(1).findFirst().getAsDouble();
		if (chance > 0.9) {
			// 10% entre 7 y 10
			offset = this.rnd.ints(0, 4).findFirst().getAsInt();
		} else if (chance < 0.4) {
			// 40% entre 3 y 7
			offset = -this.rnd.ints(0, 4).findFirst().getAsInt();
		} else {
			// 50% restante entre 7 y 9
			offset = this.rnd.ints(0, 3).findFirst().getAsInt();
		}

		return 7 + offset;
	}



}
