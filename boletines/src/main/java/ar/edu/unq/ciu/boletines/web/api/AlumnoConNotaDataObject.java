package ar.edu.unq.ciu.boletines.web.api;

import ar.edu.unq.ciu.boletines.dominio.NotaDeMateria;

public class AlumnoConNotaDataObject {

	private int nota;
	private String nombreYApellido;
	private String trimestre;
	private String materia;
	
	public AlumnoConNotaDataObject( String _nombreYap, NotaDeMateria ndm ) {
		
		this.nombreYApellido = _nombreYap;
	    this.materia = ndm.getMateria().getNombre();
	    this.nota = ndm.getNota();
	    this.trimestre = ndm.getTrimestre();
    }

	public String getMateria() {
		return materia;
	}

	public int getNota() {
		return nota;
	}

	public String getNombreYApellido() {
		return nombreYApellido;
	}

	public String getTrimestre() {
		return trimestre;
	}
	
}