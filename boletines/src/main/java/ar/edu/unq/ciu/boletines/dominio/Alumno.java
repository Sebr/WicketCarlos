package ar.edu.unq.ciu.boletines.dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import ar.edu.unq.ciu.boletines.web.api.AlumnoConNotaDataObject;

public class Alumno implements Serializable {
	
	private static final long serialVersionUID = 940012588671092139L;
	
	// atributos
	private String dni;
	private String apellido;
	private String nombre;
	private String telefono;
	private String mail;
	private Curso curso;
	private List<NotaDeMateria> notasDeMaterias = new ArrayList<>();

	// constructures
	
	public void setNotasDeMaterias(List<NotaDeMateria> notasDeMaterias) {
		this.notasDeMaterias = notasDeMaterias;
	}

	public List<NotaDeMateria> getNotasDeMaterias() {
		return notasDeMaterias;
	}

	public Alumno() {
		super();
	}
	
	public Alumno(String dni, String apellido, String nombre, String telefono, String mail) {
		super();
		this.dni = dni;
		this.apellido = apellido;
		this.nombre = nombre;
		this.telefono = telefono;
		this.mail = mail;
		
	}

	// getters y setters	
	
	public String getDni() {
		return dni;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getTelefono() {
		return telefono;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public Curso getCurso() {
		return curso;
	}
	
	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	// altas
	public void agregarNotaDeMateria(int nota, Materia materia, String trimestre){
		NotaDeMateria nm = new NotaDeMateria(materia,nota,trimestre);
		this.notasDeMaterias.add(nm);
	}
	
	// queries
	public String getNombreYApellido() {
		return this.nombre + " " + this.apellido;
	}
	
	public Integer getNotaDeMateria(Materia materia, String trimestre) {
		Optional<NotaDeMateria> notaDeMateria = this.notasDeMaterias.stream()
			.filter(nota -> (nota.getMateria().equals(materia) && nota.getTrimestre().equals(trimestre)))
			.findFirst();
		if (notaDeMateria.isPresent()) {
			return notaDeMateria.get().getNota();
		} else {
			return null;
		}
	}
	
	public NotaDeMateria getObjetoNotaDeMateria(String nombreMateria, String trimestre) {
		return this.notasDeMaterias.stream().
				filter(nm -> nm.getMateria().getNombre().
						equals(nombreMateria) && nm.getTrimestre().
						      equals(trimestre)).collect(Collectors.toList()).get(0);
	}
	
	
	public boolean existeNota(String nombreDeLaMateria) {
		return this.notasDeMaterias.stream().anyMatch(nm -> nm.getMateria().getNombre().equals(nombreDeLaMateria));
		
	}
	
	
	public AlumnoConNotaDataObject alumnoDataObject(String nombreDeLaMateria, String trimestre) {
		return new AlumnoConNotaDataObject(this.getNombreYApellido(), this.getObjetoNotaDeMateria(nombreDeLaMateria, trimestre));
               
	}
		
}
