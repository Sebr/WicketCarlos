package ar.edu.unq.ciu.boletines.altasProfesores;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;

import ar.edu.unq.ciu.boletines.logIn.LoginPage;

public class SuccessAddedProffesorPanel extends Panel {
	
	private static final long serialVersionUID = 1L;
	
	public SuccessAddedProffesorPanel(String id) {
		super(id);
		this.setVisible(false);
		this.add(this.buildForm());
	}

	
	
	
     public Form<String> buildForm(){
		
		  Form<String> newForm = new Form<String>("successForm") {
			
			private static final long serialVersionUID = 1L;
			
			   protected void onSubmit() {
			
			  this.setResponsePage(new LoginPage());
			   }		
			
		  };
	  
		  return newForm;
	

     }

}