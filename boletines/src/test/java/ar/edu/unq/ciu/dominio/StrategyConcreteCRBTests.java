package ar.edu.unq.ciu.dominio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import ar.edu.unq.ciu.boletines.dominio.StrategyConcreteCRB;

public class StrategyConcreteCRBTests {
	
	@Test
	public void testEstaAprobada() {
		// hay que sumar 19 puntos entre las tres notas,
		// todas tienen que ser 4 o más,
		// no puede haber más de una que baje de 6
		StrategyConcreteCRB cr = StrategyConcreteCRB.getInstancia();
		
		// cumple todas las condiciones
		List<Integer> notas = new ArrayList<>(Arrays.asList(4, 6, 9)); 
		assertTrue(cr.estaAprobada(notas));
		
		// no tiene notas
		notas.clear();
		notas.addAll(Arrays.asList(null, null, null));
		assertFalse(cr.estaAprobada(notas));
		
		// le falta una nota
		notas.clear();
		notas.addAll(Arrays.asList(10, 10, null)); 
		assertFalse(cr.estaAprobada(notas));
		
		// no llega a los 19 puntos
		notas.clear();
		notas.addAll(Arrays.asList(4, 6, 8)); 
		assertFalse(cr.estaAprobada(notas));

		// alguna nota es menor que 4
		notas.clear();
		notas.addAll(Arrays.asList(3, 6, 10)); 
		assertFalse(cr.estaAprobada(notas));
		
		// más de 1 nota menor que 6
		notas.clear();
		notas.addAll(Arrays.asList(5, 5, 9)); 
		assertFalse(cr.estaAprobada(notas));
	}

	@Test
	public void testEstaDesaprobada() {
		// hay que sumar 19 puntos entre las tres notas,
		// todas tienen que ser 4 o más,
		// no puede haber más de una que baje de 6
		StrategyConcreteCRB cr = StrategyConcreteCRB.getInstancia();
		
		// cumple todas las condiciones
		List<Integer> notas = new ArrayList<>(Arrays.asList(4, 6, 9)); 
		assertFalse(cr.estaDesaprobada(notas));
		
		// no tiene notas
		notas.clear();
		notas.addAll(Arrays.asList(null, null, null));
		assertFalse(cr.estaDesaprobada(notas));
		
		// alguna nota es menor que 4
		notas.clear();
		notas.addAll(Arrays.asList(3, 6, 10));
		assertTrue(cr.estaDesaprobada(notas));

		notas.clear();
		notas.addAll(Arrays.asList(10, 3, null));
		assertTrue(cr.estaDesaprobada(notas));

		notas.clear();
		notas.addAll(Arrays.asList(3, null, null));
		assertTrue(cr.estaDesaprobada(notas));

		// tiene todas las notas, pero no llega a 19 puntos
		notas.clear();
		notas.addAll(Arrays.asList(4, 6, 8));
		assertTrue(cr.estaDesaprobada(notas));

		// más de 1 nota menor que 6
		notas.clear();
		notas.addAll(Arrays.asList(5, 5, 9)); 
		assertTrue(cr.estaDesaprobada(notas));
		
		notas.clear();
		notas.addAll(Arrays.asList(5, 5, null)); 
		assertTrue(cr.estaDesaprobada(notas));
		
		// tiene 2 notas, la nota que necesitaría para aprobar es > 10
		// (con notas >= 4 y una sola < 6, tenemos como mínimo 4 + 6 = 10,
		// por lo cual no es posible que la nota requerida para llegar a 19 sea > 10)
	}
	
	@Test
	public void testQueNotaNecesitaParaAprobar() {
		// hay que sumar 19 puntos entre las tres notas,
		// todas tienen que ser 4 o más,
		// no puede haber más de una que baje de 6
		StrategyConcreteCRB cr = StrategyConcreteCRB.getInstancia();
		
		// sólo interesan los casos en que tenga 2 notas
		// (si tiene 3 notas ya está aprobada o desaprobada,
		// si tiene 1 no es suficiente para calcular qué nota necesita)
		
		// ya tiene 19 => igual necesita 4 como mínimo
		List<Integer> notas = new ArrayList<>(Arrays.asList(10, 9, null)); 
		assertEquals(cr.queNotaNecesitaParaAprobar(notas), 4);
				
		// para llegar a 19 necesita menos de 4 => igual necesita 4 como mínimo
		notas.clear();
		notas.addAll(Arrays.asList(10, 8, null)); 
		assertEquals(cr.queNotaNecesitaParaAprobar(notas), 4);

		// para llegar a 19 necesita 5, pero ya tiene una nota < 6 => necesita 6
		notas.clear();
		notas.addAll(Arrays.asList(10, 4, null)); 
		assertEquals(cr.queNotaNecesitaParaAprobar(notas), 6);

		// para llegar a 19 necesita más de 6
		notas.clear();
		notas.addAll(Arrays.asList(4, 8, null));
		assertEquals(cr.queNotaNecesitaParaAprobar(notas), 7);

		// no interesa el caso en que necesite más de 10, porque ya estaría desaprobada
	}
	
}
