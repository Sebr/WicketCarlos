package ar.edu.unq.ciu.dominio;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import ar.edu.unq.ciu.boletines.dominio.StrategyConcreteCRA;

public class StrategyConditionResolutionTests {

	@Test
	public void testLeFaltaUnaNota() {
		StrategyConcreteCRA cr = StrategyConcreteCRA.getInstancia();
		// ninguna nota
		List<Integer> notas = new ArrayList<>(Arrays.asList(null, null, null)); 
		assertFalse(cr.leFaltaUnaNota(notas));
		
		// 1 nota
		notas.clear();
		notas.addAll(Arrays.asList(0, null, null));
		assertFalse(cr.leFaltaUnaNota(notas));
		notas.clear();
		notas.addAll(Arrays.asList(null, 0, null));
		assertFalse(cr.leFaltaUnaNota(notas));
		notas.clear();
		notas.addAll(Arrays.asList(null, null, 0));
		assertFalse(cr.leFaltaUnaNota(notas));
		
		// 2 notas
		notas.clear();
		notas.addAll(Arrays.asList(null, 0, 0));
		assertTrue(cr.leFaltaUnaNota(notas));
		notas.clear();
		notas.addAll(Arrays.asList(0, null, 0));
		assertTrue(cr.leFaltaUnaNota(notas));
		notas.clear();
		notas.addAll(Arrays.asList(0, 0, null));
		assertTrue(cr.leFaltaUnaNota(notas));

		// 3 notas
		notas.clear();
		notas.addAll(Arrays.asList(0, 0, 0));
		assertFalse(cr.leFaltaUnaNota(notas));
	}

}
