package ar.edu.unq.ciu.dominio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import ar.edu.unq.ciu.boletines.dominio.StrategyConcreteCRA;

public class StrategyConcreteCRATests {

	@Test
	public void testEstaAprobada() {
		// hay que sumar 20 puntos entre las tres notas,
		// y tienen que ser todas 4 o más
		StrategyConcreteCRA cr = StrategyConcreteCRA.getInstancia();
		
		// cumple todas las condiciones
		List<Integer> notas = new ArrayList<>(Arrays.asList(4, 6, 10)); 
		assertTrue(cr.estaAprobada(notas));
		
		// no tiene notas
		notas.clear();
		notas.addAll(Arrays.asList(null, null, null));
		assertFalse(cr.estaAprobada(notas));
		
		// le falta una nota
		notas.clear();
		notas.addAll(Arrays.asList(10, 10, null)); 
		assertFalse(cr.estaAprobada(notas));
		
		// no llega a los 20 puntos
		notas.clear();
		notas.addAll(Arrays.asList(4, 6, 9)); 
		assertFalse(cr.estaAprobada(notas));

		// alguna nota es menor que 4
		notas.clear();
		notas.addAll(Arrays.asList(3, 7, 10)); 
		assertFalse(cr.estaAprobada(notas));
	}
	
	@Test
	public void testEstaDesaprobada() {
		// hay que sumar 20 puntos entre las tres notas,
		// y tienen que ser todas 4 o más
		StrategyConcreteCRA cr = StrategyConcreteCRA.getInstancia();
		
		// cumple todas las condiciones
		List<Integer> notas = new ArrayList<>(Arrays.asList(4, 6, 10)); 
		assertFalse(cr.estaDesaprobada(notas));
		
		// no tiene notas
		notas.clear();
		notas.addAll(Arrays.asList(null, null, null));
		assertFalse(cr.estaDesaprobada(notas));
		
		// alguna nota es menor que 4
		notas.clear();
		notas.addAll(Arrays.asList(10, 10, 3));
		assertTrue(cr.estaDesaprobada(notas));

		notas.clear();
		notas.addAll(Arrays.asList(10, 3, null));
		assertTrue(cr.estaDesaprobada(notas));

		notas.clear();
		notas.addAll(Arrays.asList(3, null, null));
		assertTrue(cr.estaDesaprobada(notas));

		// tiene todas las notas, pero no llega a 20 puntos
		notas.clear();
		notas.addAll(Arrays.asList(7, 6, 6));
		assertTrue(cr.estaDesaprobada(notas));

		// tiene 2 notas, la nota que necesitaría para aprobar es > 10
		notas.clear();
		notas.addAll(Arrays.asList(4, 5, null));
		assertTrue(cr.estaDesaprobada(notas));
	}

	@Test
	public void testQueNotaNecesitaParaAprobar() {
		// hay que sumar 20 puntos entre las tres notas,
		// y tienen que ser todas 4 o más
		StrategyConcreteCRA cr = StrategyConcreteCRA.getInstancia();
		
		// sólo interesan los casos en que tenga 2 notas
		// (si tiene 3 notas ya está aprobada o desaprobada,
		// si tiene 1 no es suficiente para calcular qué nota necesita)
		
		// ya tiene 20 => igual necesita 4 como mínimo
		List<Integer> notas = new ArrayList<>(Arrays.asList(10, 10, null)); 
		assertEquals(cr.queNotaNecesitaParaAprobar(notas), 4);
				
		// para llegar a 20 necesita menos de 4 => igual necesita 4 como mínimo
		notas.clear();
		notas.addAll(Arrays.asList(10, 9, null)); 
		assertEquals(cr.queNotaNecesitaParaAprobar(notas), 4);

		// para llegar a 20 necesita más de 4
		notas.clear();
		notas.addAll(Arrays.asList(10, 5, null)); 
		assertEquals(cr.queNotaNecesitaParaAprobar(notas), 5);
		
		// no interesa el caso en que necesite más de 10, porque ya estaría desaprobada
	}

}
